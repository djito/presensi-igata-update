-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 07, 2023 at 04:00 PM
-- Server version: 5.7.33
-- PHP Version: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `presensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_users`
--

CREATE TABLE `api_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `api_users`
--

INSERT INTO `api_users` (`id`, `username`, `password_digest`) VALUES
(1, 'presensi-xyz', '$2a$12$AVoQ/u5EZPKXe/hjP9nVK.66lxXlan1h.lVRKf0EVU1zBeTyMJnbe');

-- --------------------------------------------------------

--
-- Table structure for table `cluster`
--

CREATE TABLE `cluster` (
  `id` int(4) NOT NULL,
  `nama_cluster` varchar(25) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cluster`
--

INSERT INTO `cluster` (`id`, `nama_cluster`, `keterangan`, `created_at`, `updated_at`) VALUES
(5, 'Tetap edit', 'Karyawan Tetap edit', '2023-06-08 06:06:52', '2023-06-08 07:12:13');

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE `departemen` (
  `id` int(4) NOT NULL,
  `nama_dep` varchar(25) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`id`, `nama_dep`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'IT', 'IT', '2023-06-08 07:53:10', '2023-07-19 19:37:27'),
(2, 'Keuangan', 'keungan', '2023-07-02 21:31:42', '2023-07-19 19:38:57'),
(3, 'Cleaning Service', 'Cleaning Service', '2023-07-02 21:32:16', '2023-07-19 19:39:12'),
(4, 'Scurity', 'Scurity', '2023-07-19 19:39:34', '2023-07-19 19:39:34'),
(5, 'Manajemen', 'Manajemen', '2023-07-19 19:39:45', '2023-07-19 19:39:45');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(4) NOT NULL,
  `nama_jabatan` varchar(25) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama_jabatan`, `keterangan`, `created_at`, `updated_at`) VALUES
(6, 'Direktur', 'Direktur', '2023-07-19 20:18:41', '2023-07-19 20:18:41'),
(7, 'Manajer', 'Manajer IT 1', '2023-07-19 20:31:17', '2023-07-19 20:31:17'),
(8, 'Manajer', 'Manajer IT 2', '2023-07-19 20:31:26', '2023-07-19 20:31:26'),
(9, 'Staff', 'Staff', '2023-07-19 20:57:44', '2023-07-19 20:57:44'),
(10, 'Admin', 'Admin', '2023-07-20 00:30:35', '2023-07-20 00:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(4) NOT NULL,
  `nama_level` varchar(25) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `nama_level`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 'Admin', 'Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Manajer', 'Manajer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Staff', 'Staff', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_latitude` double DEFAULT NULL,
  `address_longitude` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_19_055054_create_location_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` char(100) CHARACTER SET utf8 NOT NULL,
  `token` char(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tb_arsip_users`
--

CREATE TABLE `tb_arsip_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nik` bigint(20) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gelar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_ktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_domisili` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` bigint(20) NOT NULL,
  `level` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_view` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cuti`
--

CREATE TABLE `tb_cuti` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `cuti_tahunan` int(11) NOT NULL,
  `cuti_berjalan` int(11) NOT NULL,
  `cuti_bersama` int(11) NOT NULL,
  `cuti_lain` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_cuti`
--

INSERT INTO `tb_cuti` (`id`, `id_user`, `cuti_tahunan`, `cuti_berjalan`, `cuti_bersama`, `cuti_lain`, `created_at`, `updated_at`) VALUES
(1, 123456, 10, 0, 4, 0, '2022-05-20 08:45:23', '2022-05-20 08:45:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_header`
--

CREATE TABLE `tb_header` (
  `id` int(11) NOT NULL,
  `logo` varchar(50) CHARACTER SET utf8 NOT NULL,
  `yayasan` varchar(100) CHARACTER SET utf8 NOT NULL,
  `unit` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lat` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lng` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_header`
--

INSERT INTO `tb_header` (`id`, `logo`, `yayasan`, `unit`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, '24588.png', '', 'IT Shop Purwokerto', '-6.255946161014403', '106.72544539139898', '2021-04-13 14:28:50', '2023-07-04 05:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jammasuk`
--

CREATE TABLE `tb_jammasuk` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `masuk_senin` time NOT NULL,
  `masuk_selasa` time NOT NULL,
  `masuk_rabu` time NOT NULL,
  `masuk_kamis` time NOT NULL,
  `masuk_jumat` time NOT NULL,
  `masuk_sabtu` time NOT NULL,
  `masuk_minggu` time NOT NULL,
  `wf1` varchar(10) CHARACTER SET utf8 NOT NULL,
  `wf2` varchar(10) CHARACTER SET utf8 NOT NULL,
  `wf3` varchar(10) CHARACTER SET utf8 NOT NULL,
  `wf4` varchar(10) CHARACTER SET utf8 NOT NULL,
  `wf5` varchar(20) CHARACTER SET utf8 NOT NULL,
  `wf6` varchar(10) CHARACTER SET utf8 NOT NULL,
  `wf7` varchar(10) CHARACTER SET utf8 NOT NULL,
  `keluar_senin` time NOT NULL,
  `keluar_selasa` time NOT NULL,
  `keluar_rabu` time NOT NULL,
  `keluar_kamis` time NOT NULL,
  `keluar_jumat` time NOT NULL,
  `keluar_sabtu` time NOT NULL,
  `keluar_minggu` time NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_jammasuk`
--

INSERT INTO `tb_jammasuk` (`id`, `id_user`, `masuk_senin`, `masuk_selasa`, `masuk_rabu`, `masuk_kamis`, `masuk_jumat`, `masuk_sabtu`, `masuk_minggu`, `wf1`, `wf2`, `wf3`, `wf4`, `wf5`, `wf6`, `wf7`, `keluar_senin`, `keluar_selasa`, `keluar_rabu`, `keluar_kamis`, `keluar_jumat`, `keluar_sabtu`, `keluar_minggu`, `created_at`, `updated_at`) VALUES
(1, 123456, '07:35:00', '07:35:00', '07:35:00', '07:35:00', '07:35:00', '00:00:00', '00:00:00', 'OFF', 'WFH', 'WFH', 'OFF', 'WFH', 'OFF', 'OFF', '16:00:00', '16:00:00', '16:00:00', '16:00:00', '16:00:00', '00:00:00', '00:00:00', '2022-02-08 05:37:08', '2020-12-25 15:55:55'),
(2, 3171032406780006, '08:00:00', '08:00:00', '09:00:00', '08:00:00', '08:00:00', '00:00:00', '00:00:00', 'WFH', 'WFH', 'WFH', 'WFH', 'WFO', 'OFF', 'OFF', '17:00:00', '17:00:00', '17:00:00', '17:00:00', '17:00:00', '00:00:00', '00:00:00', '2023-05-22 09:37:35', '2023-05-22 09:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jammasuk_kerja`
--

CREATE TABLE `tb_jammasuk_kerja` (
  `id` int(11) NOT NULL,
  `id_shift_kerja` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_lembur` int(11) NOT NULL,
  `id_shift` int(11) NOT NULL,
  `tglmasuk` date NOT NULL,
  `jammasuk` time NOT NULL,
  `jampulang` time NOT NULL,
  `wf` varchar(10) NOT NULL,
  `lembur` varchar(20) NOT NULL,
  `jml_jamkerja` time NOT NULL,
  `jml_lembur` time NOT NULL,
  `lokasi` varchar(200) NOT NULL,
  `id_atasan` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jammasuk_kerja`
--

INSERT INTO `tb_jammasuk_kerja` (`id`, `id_shift_kerja`, `id_user`, `id_lembur`, `id_shift`, `tglmasuk`, `jammasuk`, `jampulang`, `wf`, `lembur`, `jml_jamkerja`, `jml_lembur`, `lokasi`, `id_atasan`, `created_at`, `updated_at`) VALUES
(125, 12, 3674034304550001, 0, 0, '2023-08-02', '07:00:00', '18:00:00', 'WFH', '', '00:00:00', '00:00:00', '', 0, '2023-08-01 09:37:43', '2023-08-01 09:37:43'),
(126, 12, 111, 0, 0, '2023-08-10', '07:00:00', '18:00:00', 'WFO', '', '00:00:00', '00:00:00', '', 0, '2023-08-09 17:10:37', '2023-08-09 17:10:37'),
(127, 15, 111, 16, 0, '2023-08-13', '07:00:00', '16:00:00', 'WFO', '', '00:00:00', '08:00:00', '', 0, '2023-08-11 10:35:25', '2023-08-11 10:53:09'),
(128, 13, 111, 17, 0, '2023-08-14', '09:00:00', '18:00:00', 'WFO', '', '00:00:00', '00:00:00', '', 0, '2023-08-11 10:35:41', '2023-08-11 13:20:40'),
(129, 12, 111, 0, 0, '2023-08-15', '07:00:00', '18:00:00', 'WFO', '', '00:00:00', '00:00:00', '', 0, '2023-08-11 13:38:48', '2023-08-11 13:38:48'),
(130, 12, 3674034304550005, 0, 0, '2023-09-08', '07:00:00', '18:00:00', 'WFO', '', '00:00:00', '00:00:00', '', 0, '2023-09-07 20:24:54', '2023-09-07 20:24:54'),
(131, 15, 3674034304550005, 1, 0, '2023-09-09', '07:00:00', '16:00:00', 'OFF', '', '00:00:00', '08:00:00', '', 0, '2023-09-07 20:25:06', '2023-09-07 20:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jam_shift_kerja`
--

CREATE TABLE `tb_jam_shift_kerja` (
  `id` int(11) NOT NULL,
  `id_shift_kerja` int(11) NOT NULL,
  `tglmasuk` date NOT NULL,
  `jammasuk` time NOT NULL,
  `jampulang` time NOT NULL,
  `wf` varchar(10) NOT NULL,
  `lembur` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jam_shift_kerja`
--

INSERT INTO `tb_jam_shift_kerja` (`id`, `id_shift_kerja`, `tglmasuk`, `jammasuk`, `jampulang`, `wf`, `lembur`, `created_at`, `updated_at`) VALUES
(1, 3, '2023-06-01', '08:00:00', '16:00:00', 'WFH', '', '2023-05-25 23:58:36', '2023-05-31 09:29:14'),
(2, 3, '2023-06-02', '08:00:00', '16:00:00', 'WFO', '', '2023-05-25 23:59:50', '2023-05-25 23:59:50'),
(3, 4, '2023-06-01', '08:00:00', '16:00:00', 'WFH', '', '2023-05-26 00:00:42', '2023-05-26 00:00:42'),
(5, 3, '2023-06-07', '08:00:00', '16:00:00', 'WFO', '', '2023-05-26 00:04:07', '2023-05-26 00:04:07'),
(6, 3, '2023-06-04', '00:00:00', '00:00:00', 'OFF', '', '2023-05-26 00:04:35', '2023-05-26 00:04:35'),
(7, 3, '2023-05-30', '08:00:00', '16:00:00', 'WFH', '', '2023-05-30 07:57:15', '2023-05-31 09:29:04'),
(8, 4, '2023-06-02', '07:00:00', '15:00:00', 'WFO', '', '2023-05-31 08:22:40', '2023-05-31 08:22:40'),
(9, 4, '2023-06-10', '00:00:00', '00:00:00', 'OFF', '', '2023-05-31 16:46:38', '2023-05-31 16:46:38'),
(10, 3, '2023-05-31', '00:00:00', '00:00:00', 'OFF', '', '2023-05-31 22:17:53', '2023-05-31 22:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jeniscuti`
--

CREATE TABLE `tb_jeniscuti` (
  `id` int(11) NOT NULL,
  `jenis_cuti` varchar(100) CHARACTER SET utf8 NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_jeniscuti`
--

INSERT INTO `tb_jeniscuti` (`id`, `jenis_cuti`, `keterangan`, `created_at`, `updated_at`) VALUES
(3, 'Cuti Berjalan', 'Cuti yang di berikan ketika cuti tahunan habis, cuti di perbolehkan asal ada alasan yang kuat', '2022-04-19 13:55:43', '2020-06-28 13:11:36'),
(4, 'Cuti Lain', 'Cuti yang sifatnya kondisional, seperti cuti sakit dan cuti melahirkan', '2022-04-19 13:55:40', '2020-06-28 13:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengajuan_cuti`
--

CREATE TABLE `tb_pengajuan_cuti` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_cuti` int(11) NOT NULL,
  `id_pengganti` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `dari_tanggal` date NOT NULL,
  `sampai_tanggal` date NOT NULL,
  `admin` int(2) NOT NULL,
  `atasan` int(2) NOT NULL,
  `alasan` text CHARACTER SET utf8 NOT NULL,
  `bukti_pendukung` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 NOT NULL,
  `disetujui` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pengajuan_cuti`
--

INSERT INTO `tb_pengajuan_cuti` (`id`, `id_user`, `id_cuti`, `id_pengganti`, `tanggal`, `dari_tanggal`, `sampai_tanggal`, `admin`, `atasan`, `alasan`, `bukti_pendukung`, `status`, `disetujui`, `created_at`, `updated_at`) VALUES
(14, 111, 3, 3, '0000-00-00', '2023-07-24', '2023-07-29', 0, 1, 'coba 2', '-', 'di tolak', 'sujito, 21-07-2023, 18:38:08', NULL, NULL),
(15, 111, 3, 8, '0000-00-00', '2023-07-26', '2023-07-28', 1, 1, 'coba', '-', 'di terima', 'Admin, 24-07-2023, 14:04:12', NULL, NULL),
(16, 111, 4, 9, '0000-00-00', '2023-08-01', '2023-08-02', 1, 1, 'coba lagi', '-', 'di terima', 'Disetujui sujito, 24-07-2023, 14:08:54<br>Admin, 24-07-2023, 14:09:09', NULL, NULL),
(17, 111, 3, 8, '0000-00-00', '2023-07-31', '2023-08-02', 1, 1, '', '-', 'di terima', 'Disetujui sujito, 25-07-2023, 09:54:31. Disetujui Admin, 25-07-2023, 09:55:48', NULL, NULL),
(18, 111, 3, 4, '0000-00-00', '2023-08-10', '2023-07-15', 1, 1, '', '-', 'di terima', 'Disetujui sujito, 25-07-2023, 10:55:03. Disetujui Admin, 25-07-2023, 10:55:59', NULL, NULL),
(19, 111, 3, 8, '0000-00-00', '2023-08-13', '2023-08-14', 1, 1, 'coba', '-', 'di terima', 'Disetujui sujito, 25-07-2023, 10:58:38. Disetujui Admin, 25-07-2023, 11:03:38', NULL, NULL),
(20, 111, 4, 4, '0000-00-00', '2023-08-16', '2023-08-17', 1, 1, 'cek', '-', 'di terima', 'Disetujui sujito, 25-07-2023, 11:13:29. Disetujui Admin, 25-07-2023, 11:13:38', NULL, NULL),
(21, 111, 3, 4, '0000-00-00', '2023-08-04', '2023-08-04', 1, 1, 'cuti', '-', 'di terima', 'Disetujui sujito, 25-07-2023, 17:00:51. Disetujui Admin, 25-07-2023, 17:01:00', NULL, NULL),
(22, 3674034304550005, 3, 3, '0000-00-00', '2023-09-08', '2023-09-09', 1, 1, 'cuti contoh 1', '-', 'di terima', 'Disetujui sujito, 07-09-2023, 20:21:25. Disetujui Admin, 07-09-2023, 20:22:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengajuan_izin`
--

CREATE TABLE `tb_pengajuan_izin` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `tanggal` date NOT NULL,
  `dari_tanggal` date NOT NULL,
  `sampai_tanggal` date NOT NULL,
  `id_pengganti` int(11) NOT NULL,
  `jenis_izin` varchar(30) CHARACTER SET utf8 NOT NULL,
  `alasan` text CHARACTER SET utf8 NOT NULL,
  `bukti_pendukung` text CHARACTER SET utf8 NOT NULL,
  `atasan` int(2) NOT NULL,
  `admin` int(2) NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 NOT NULL,
  `disetujui` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pengajuan_izin`
--

INSERT INTO `tb_pengajuan_izin` (`id`, `id_user`, `tanggal`, `dari_tanggal`, `sampai_tanggal`, `id_pengganti`, `jenis_izin`, `alasan`, `bukti_pendukung`, `atasan`, `admin`, `status`, `disetujui`, `created_at`, `updated_at`) VALUES
(5, 3171032406780006, '2023-07-25', '0000-00-00', '0000-00-00', 4, 'D', 'coba 2', '-', 0, 0, 'pengajuan', '', NULL, NULL),
(6, 3171032406780006, '2023-07-26', '0000-00-00', '0000-00-00', 4, 'D', 'coba 2', '-', 0, 0, 'pengajuan', '', NULL, NULL),
(7, 3171032406780006, '2023-07-28', '0000-00-00', '0000-00-00', 4, 'D', 'coba 2', '-', 0, 0, 'di terima', '', NULL, NULL),
(9, 111, '0000-00-00', '2023-07-27', '2023-07-28', 8, 'I1', 'coba lagi', '-', 1, 1, 'di terima', 'Disetujui sujito, 25-07-2023, 11:48:50. Disetujui Admin, 25-07-2023, 11:49:22', NULL, NULL),
(10, 111, '0000-00-00', '2023-07-26', '2023-07-26', 4, 'D', 'cooba', '-', 0, 0, 'di tolak', 'menunggu persetujuan Atasan dan Admin', NULL, NULL),
(11, 111, '0000-00-00', '2023-07-27', '2023-07-27', 4, 'D', 'cobaaa', '-', 1, 0, 'di tolak', 'sujito, 25-07-2023, 12:55:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengajuan_lembur`
--

CREATE TABLE `tb_pengajuan_lembur` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_jam` int(11) NOT NULL,
  `tgllembur` date NOT NULL,
  `jammasuk` time NOT NULL,
  `jampulang` time NOT NULL,
  `id_shift` int(11) NOT NULL,
  `sebelum_shift` time NOT NULL,
  `setelah_shift` time NOT NULL,
  `istirahat_sebelum` time NOT NULL,
  `istirahat_setelah` time NOT NULL,
  `jumlah_lembur` time NOT NULL,
  `kompensasi` int(2) NOT NULL,
  `admin` int(2) NOT NULL,
  `atasan` int(2) NOT NULL,
  `bukti_pendukung` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `disetujui` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengajuan_lembur`
--

INSERT INTO `tb_pengajuan_lembur` (`id`, `id_user`, `id_jam`, `tgllembur`, `jammasuk`, `jampulang`, `id_shift`, `sebelum_shift`, `setelah_shift`, `istirahat_sebelum`, `istirahat_setelah`, `jumlah_lembur`, `kompensasi`, `admin`, `atasan`, `bukti_pendukung`, `status`, `disetujui`, `created_at`, `updated_at`) VALUES
(1, 3674034304550005, 131, '2023-09-09', '07:00:00', '16:00:00', 0, '06:00:00', '19:00:00', '00:00:00', '00:00:00', '08:00:00', 1, 1, 1, '-', 'di terima', 'Disetujui sujito, 07-09-2023, 20:29:58. Disetujui Admin, 07-09-2023, 20:30:07', '2023-09-07 20:29:07', '2023-09-07 20:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `gambar` varchar(50) CHARACTER SET utf8 NOT NULL,
  `isi` text CHARACTER SET utf8 NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `view` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id`, `judul`, `url`, `gambar`, `isi`, `tanggal`, `id_user`, `view`, `created_at`, `updated_at`) VALUES
(1, 'Langkah melakukan presensi', 'Langkah_melakukan_presensi', 'presensi-2.jpg', '<p>Langkah melakukan presensi</p><ol><li>Izinkan lokasi / Allow Location</li><li>Pilih tombol berangkat untuk presensi berangkat</li><li>Pilih tombol pulang untuk presensi pulang</li><li>Masukan NIK yang telah terdaftar untuk melakukan presensi</li><li>Selesai</li></ol>', '2020-07-15', 1, 166, '2020-12-25 18:34:38', '2020-07-15 01:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_presensi`
--

CREATE TABLE `tb_presensi` (
  `id` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `tanggal` date NOT NULL,
  `berangkat` time NOT NULL,
  `lokasi_berangkat` varchar(200) CHARACTER SET utf8 NOT NULL,
  `pulang` time NOT NULL,
  `lokasi_pulang` varchar(200) CHARACTER SET utf8 NOT NULL,
  `hardware` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ip` varchar(100) CHARACTER SET utf8 NOT NULL,
  `swafoto1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `swafoto2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `keterangan` int(11) NOT NULL,
  `keterangan_rinci` varchar(200) CHARACTER SET utf8 NOT NULL,
  `keterangan_kerja` varchar(50) CHARACTER SET utf8 NOT NULL,
  `keterangan_presensi` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `laporan_wfh` text CHARACTER SET utf8,
  `laporan_wfo` text COLLATE utf8_unicode_ci NOT NULL,
  `id_session` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_session_pulang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cuti` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_presensi`
--

INSERT INTO `tb_presensi` (`id`, `id_user`, `tanggal`, `berangkat`, `lokasi_berangkat`, `pulang`, `lokasi_pulang`, `hardware`, `ip`, `swafoto1`, `swafoto2`, `keterangan`, `keterangan_rinci`, `keterangan_kerja`, `keterangan_presensi`, `laporan_wfh`, `laporan_wfo`, `id_session`, `id_session_pulang`, `cuti`, `created_at`, `updated_at`) VALUES
(1, 3171032406780006, '2023-05-22', '16:38:04', '-6.2560426666666675,106.72540566666666', '00:00:00', '', 'Laptop', '127.0.0.1', '25588.jfif', NULL, 0, '', 'WFH', '', NULL, '', 'foX7RxUCorcnPywYRgU30A1lf9gTMIptvDbSajV3/2023-05-22/datang', '', NULL, '2023-05-22 16:38:04', '2023-05-22 16:38:04'),
(2, 3171032406780006, '2023-05-23', '17:18:06', '-6.2560171,106.7254004', '00:00:00', '', 'Laptop', '127.0.0.1', '62481.png', NULL, 0, '', 'WFH', '', NULL, '', 'C073bSdoDE7DSteoegZd43hwbVjrc0Xukyz0wQ4d/2023-05-23/datang', '', NULL, '2023-05-23 17:18:06', '2023-05-23 17:18:06'),
(3, 3171032406780006, '2023-05-29', '22:24:50', '-6.256137,106.725433', '00:00:00', '', 'Laptop', '127.0.0.1', '44017.png', NULL, 0, '', 'WFH', '', NULL, '', 'J6XUVly8FnvCk4vCWglLA5w0C5S0trCx8QgY1gyU/2023-05-29/datang', '', NULL, '2023-05-29 22:24:50', '2023-05-29 22:24:50'),
(5, 3171032406780006, '2023-05-30', '08:00:58', '-6.3428,106.7526', '08:32:20', '-6.3428,106.7526', 'Laptop', '127.0.0.1', '19049.png', '79789.png', 0, '', 'WFH', '', '', '', 'uNYNgxJwrZD6vf83Lr9X5HzQqDVSZFwRHQx6DuY5/2023-05-30/datang', 'uNYNgxJwrZD6vf83Lr9X5HzQqDVSZFwRHQx6DuY5/2023-05-30/pulang', NULL, '2023-05-30 08:00:58', '2023-05-30 08:00:58'),
(6, 3171032406780006, '2023-06-08', '17:15:35', '-6.2324736,106.758144', '00:00:00', '', 'Laptop', '127.0.0.1', '57886.png', NULL, 0, '', 'WFH', '', NULL, '', 'TjYWIr894dhxcjzjo5xHaZqp2XPe1dM7wMCk9nMq/2023-06-08/datang', '', NULL, '2023-06-08 17:15:35', '2023-06-08 17:15:35'),
(7, 123456, '2022-05-25', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '1', '2023-06-27 06:37:36', '2023-06-27 06:37:36'),
(13, 3171032406780006, '2023-07-06', '16:48:34', '-6.256085655004106,106.72542407877931', '16:48:51', '-6.2561444999999996,106.7254275', 'Laptop', '127.0.0.1', '33357.png', '82224.png', 0, '', 'WFH', '', 'laporan wfh', '', 'wrLZDLHgcKmM44IAPjZzb22DX7NRxWtRUUXFnHI9/2023-07-06/datang', 'wrLZDLHgcKmM44IAPjZzb22DX7NRxWtRUUXFnHI9/2023-07-06/pulang', NULL, '2023-07-06 16:48:34', '2023-07-06 16:48:34'),
(14, 3171032406780006, '2023-07-28', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', 'D', '2023-07-21 07:46:03', '2023-07-21 07:46:03'),
(20, 111, '0000-00-00', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '3', '2023-07-25 09:55:48', '2023-07-25 09:55:48'),
(23, 111, '2023-07-25', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '3', '2023-07-25 10:59:01', '2023-07-25 10:59:01'),
(24, 111, '2023-07-25', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '3', '2023-07-25 11:03:38', '2023-07-25 11:03:38'),
(25, 111, '2023-07-25', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '3', '2023-07-25 11:03:38', '2023-07-25 11:03:38'),
(26, 111, '2023-08-16', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '4', '2023-07-25 11:13:38', '2023-07-25 11:13:38'),
(27, 111, '2023-08-17', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', '4', '2023-07-25 11:13:38', '2023-07-25 11:13:38'),
(28, 111, '0000-00-00', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', 'D', '2023-07-25 04:36:58', '2023-07-25 04:36:58'),
(29, 111, '2023-07-27', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', 'I1', '2023-07-25 11:49:22', '2023-07-25 11:49:22'),
(30, 111, '2023-07-28', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', '', NULL, '', '', '', 'I1', '2023-07-25 11:49:22', '2023-07-25 11:49:22'),
(31, 111, '2023-07-28', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', 'Izin Telat', NULL, '', '', '', 'I1', '2023-07-25 16:29:58', '2023-07-25 16:29:58'),
(32, 111, '2023-08-04', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', 'Cuti Bersama', NULL, '', '', '', '3', '2023-07-25 17:01:00', '2023-07-25 17:01:00'),
(33, 3674034304550005, '2023-09-08', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', 'Cuti Bersama', NULL, '', '', '', '3', '2023-09-07 20:22:07', '2023-09-07 20:22:07'),
(34, 3674034304550005, '2023-09-09', '00:00:00', '', '00:00:00', '', '', '', NULL, NULL, 0, '', '', 'Cuti Bersama', NULL, '', '', '', '3', '2023-09-07 20:22:07', '2023-09-07 20:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_radius`
--

CREATE TABLE `tb_radius` (
  `id` int(11) NOT NULL,
  `lat_atas` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lat_bawah` varchar(50) CHARACTER SET utf8 NOT NULL,
  `long_atas` varchar(50) CHARACTER SET utf8 NOT NULL,
  `long_bawah` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_radius`
--

INSERT INTO `tb_radius` (`id`, `lat_atas`, `lat_bawah`, `long_atas`, `long_bawah`, `created_at`, `updated_at`) VALUES
(1, '-6.255663590685383', '-6.256372681976441', '106.72542929250118', '106.72537026926415', '2022-04-19 13:57:15', '2021-04-13 14:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_registrasi`
--

CREATE TABLE `tb_registrasi` (
  `id` int(11) NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `gelar_depan` varchar(30) CHARACTER SET utf8 NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `gelar_belakang` varchar(30) CHARACTER SET utf8 NOT NULL,
  `institusi` varchar(100) CHARACTER SET utf8 NOT NULL,
  `no_hp` varchar(30) CHARACTER SET utf8 NOT NULL,
  `informasi` varchar(100) CHARACTER SET utf8 NOT NULL,
  `pekerjaan` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tb_shift_kerja`
--

CREATE TABLE `tb_shift_kerja` (
  `id` int(11) NOT NULL,
  `nama_shift` varchar(50) NOT NULL,
  `jammasuk` time NOT NULL,
  `jampulang` time NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_shift_kerja`
--

INSERT INTO `tb_shift_kerja` (`id`, `nama_shift`, `jammasuk`, `jampulang`, `keterangan`, `created_at`, `updated_at`) VALUES
(12, 'Pagi', '07:00:00', '18:00:00', 'scurity pagi 1', '2023-07-27 18:08:19', '2023-07-27 18:08:19'),
(13, 'Pagi', '09:00:00', '18:00:00', 'security pagi 2', '2023-07-31 01:46:25', '2023-07-31 01:46:25'),
(14, 'Malam', '19:00:00', '07:00:00', 'security Malam', '2023-07-31 01:47:01', '2023-07-31 01:47:01'),
(15, 'Off', '00:00:00', '00:00:00', 'off', '2023-07-31 01:48:10', '2023-07-31 01:48:10'),
(16, 'Pagi', '07:00:00', '16:00:00', 'security pagi 1', '2023-08-01 13:55:57', '2023-08-01 13:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id` bigint(20) NOT NULL,
  `nik` bigint(20) NOT NULL,
  `id_atasan` bigint(20) NOT NULL,
  `id_atasan1` int(4) NOT NULL,
  `id_dep` int(4) NOT NULL,
  `id_jabatan` int(4) NOT NULL,
  `id_cluster` int(4) NOT NULL,
  `id_level` int(4) NOT NULL,
  `status` int(2) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_view` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gelar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_ktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_domisili` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` bigint(20) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_shift` int(11) NOT NULL,
  `cluster` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id`, `nik`, `id_atasan`, `id_atasan1`, `id_dep`, `id_jabatan`, `id_cluster`, `id_level`, `status`, `nama`, `username`, `email`, `password`, `password_view`, `gelar`, `jabatan`, `alamat_ktp`, `alamat_domisili`, `no_hp`, `remember_token`, `unit`, `level`, `id_shift`, `cluster`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 6, 5, 10, 0, 2, 1, 'Admin', 'admin', 'admin@gmail.com', '$2y$10$7htvZswvbDeeaieBCtiYGu.5XFps3M8m8MnCaFFjRZ6M99s4K3tH2', '1', '', '', '', '', 0, 'iYMbU3WISvdlGCFb7lZXdBiSIVcRz8X2upEwa4agUUmU8icCvO67uxQUMtbg', '', '0', 0, 'OB', NULL, '2023-07-20 07:33:33'),
(2, 123456, 123456, 3171032406780006, 0, 1, 0, 5, 6, 0, 'Fadli Prasetyo', 'johnthor', 'johnthor@gmail.com', '$2y$10$pCN1Ievyd/KFh6ZQUZPFMOpEuLA0eFoZSLXNqlDcW4melJ.vpIu4i', '123456', '', '', 'Jl. SD Inpres Pabuaran Barat', 'Jl. SD Inpres Pabuaran Barat', 0, 'i9O3VRwbbhpdvID9tdv91SQlEuLjt7CdKnXkKh97SW1RwoTMHsBk9hfcBas9', '', '5', 0, 'Tetap', '2020-12-25 10:54:10', '2023-06-27 06:34:36'),
(3, 3171032406780006, 3171032406780006, 1, 6, 1, 7, 5, 3, 1, 'sujito', 'djito', 'djito.ubl@gmail.com', '$2y$10$pMEHi.sWdPQADux6cCFCOujC3XbrUzEKEyx2YgHtotHAwVNpxf1b2', 'happy240678', '', '', '', '', 0, NULL, '', '1', 0, 'Tetap', '2023-05-22 09:27:55', '2023-07-20 17:37:51'),
(4, 3674034304550005, 3674034304550005, 3171032406780006, 7, 1, 9, 5, 6, 1, 'GALIH PERKASA', 'test', 'test@test.com', '$2y$10$2fzoo.eXmwCcwgKCZ/Yr4OOFgcjdzVPDoX1irJl4WMglRAQxsHDJe', '12345678', '', '', 'JL. SD INPRES PABUARAN BARAT', 'JL. SD INPRES PABUARAN BARAT', 0, NULL, '', '5', 0, 'OB', '2023-05-23 12:40:49', '2023-07-21 11:51:10'),
(5, 1991919191199119, 1991919191199119, 3171032406780006, 0, 1, 0, 5, 6, 0, 'FIBRIANA SURYA NINGSIH', 'HMY', 'test@test.com', '$2y$10$eo8DjteOPo4Q7pD8rJczNu8j1H/xQyaFWwoV9VnU/gILbIFzbdJUm', '12345678', '', '', 'JL. SD INPRES PABUARAN BARAT', 'JL. SD INPRES PABUARAN BARAT', 0, NULL, '', '1', 0, 'Tetap', '2023-05-31 10:38:12', '2023-06-27 06:34:46'),
(6, 3674036303810004, 3674036303810004, 3171032406780006, 0, 1, 0, 5, 6, 0, 'Endang Wiharti', 'test1@test.com', 'test1@test.com', '$2y$10$tFEDEh3S.fDXq9eGvZlpNeo6XtTwMYKG.UvafW9Oh0z11aL7TAKjm', '12345678', '', '', 'Jl. Raya Ceger', 'Jl. Raya Ceger', 231121121111111, NULL, '', '', 0, '', '2023-06-26 16:18:24', '2023-06-27 06:35:13'),
(7, 3674031412830003, 3674031412830003, 0, 10, 5, 9, 5, 6, 1, 'RAYHAN AKBAR', 'test2@test.com', 'test2@test.com', '$2y$10$JEum7ni4Pls3F6X05xlsi.xQRQtIvCxfWiv4MsaMkPt4PSuVb8cGi', '12345678', '', '9', 'JL. SD INPRES PABUARAN BARAT', 'JL. SD INPRES PABUARAN BARAT', 856921343333, NULL, '', '', 0, '', '2023-06-26 16:36:51', '2023-07-20 10:12:50'),
(8, 3674034304550001, 3674034304550001, 3171032406780006, 6, 1, 8, 0, 3, 1, 'djito', 'djito1', 'djito.ubl1@gmail.com', '$2y$10$PYbD616qpq2px1GMpRjMhe9Sl2AzjiY0EBXufG9jLRgVQr/aL0PRm', '12345', '', '', '', '', 0, NULL, '', '', 0, '', '2023-07-11 17:02:33', '2023-07-20 07:34:55'),
(9, 111, 111, 0, 7, 1, 9, 0, 6, 1, 'sprei', 'sprei', 'sprei.galleri@gmail.com', '$2y$10$VtGa/DFCTI6pOp1HwAqBWOL8ceHFwJMRyhSNyjNDsOD1cMU2TqyDu', '123', '', '9', '', '', 0, NULL, '', '', 0, '', '2023-07-20 03:59:15', '2023-07-20 09:15:27'),
(10, 121, 121, 0, 6, 1, 7, 0, 3, 1, 'sprei1', 'sprei1', 'galery.sprei@gmail.com', '$2y$10$CRrTINH1ju/aPl7slC2fSOckpP7awUqMwZlTq7zlb4E3Dg/R0Vi6q', '123', '', '', '', '', 0, NULL, '', '', 0, '', '2023-07-20 04:32:23', '2023-07-20 09:14:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_users`
--
ALTER TABLE `api_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `cluster`
--
ALTER TABLE `cluster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_arsip_users`
--
ALTER TABLE `tb_arsip_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_cuti`
--
ALTER TABLE `tb_cuti`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_header`
--
ALTER TABLE `tb_header`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_jammasuk`
--
ALTER TABLE `tb_jammasuk`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_jammasuk_kerja`
--
ALTER TABLE `tb_jammasuk_kerja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_lembur` (`id_lembur`);

--
-- Indexes for table `tb_jam_shift_kerja`
--
ALTER TABLE `tb_jam_shift_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jeniscuti`
--
ALTER TABLE `tb_jeniscuti`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_pengajuan_cuti`
--
ALTER TABLE `tb_pengajuan_cuti`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_pengajuan_izin`
--
ALTER TABLE `tb_pengajuan_izin`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_pengajuan_lembur`
--
ALTER TABLE `tb_pengajuan_lembur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_radius`
--
ALTER TABLE `tb_radius`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_registrasi`
--
ALTER TABLE `tb_registrasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_shift_kerja`
--
ALTER TABLE `tb_shift_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`) USING BTREE,
  ADD UNIQUE KEY `nik` (`nik`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_level` (`id_level`),
  ADD KEY `status` (`status`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_atasan1` (`id_atasan1`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_users`
--
ALTER TABLE `api_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cluster`
--
ALTER TABLE `cluster`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `departemen`
--
ALTER TABLE `departemen`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_arsip_users`
--
ALTER TABLE `tb_arsip_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_cuti`
--
ALTER TABLE `tb_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_header`
--
ALTER TABLE `tb_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_jammasuk`
--
ALTER TABLE `tb_jammasuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_jammasuk_kerja`
--
ALTER TABLE `tb_jammasuk_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tb_jam_shift_kerja`
--
ALTER TABLE `tb_jam_shift_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_jeniscuti`
--
ALTER TABLE `tb_jeniscuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_pengajuan_cuti`
--
ALTER TABLE `tb_pengajuan_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_pengajuan_izin`
--
ALTER TABLE `tb_pengajuan_izin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_pengajuan_lembur`
--
ALTER TABLE `tb_pengajuan_lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tb_radius`
--
ALTER TABLE `tb_radius`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_registrasi`
--
ALTER TABLE `tb_registrasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_shift_kerja`
--
ALTER TABLE `tb_shift_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_cuti`
--
ALTER TABLE `tb_cuti`
  ADD CONSTRAINT `tb_cuti_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_jammasuk`
--
ALTER TABLE `tb_jammasuk`
  ADD CONSTRAINT `tb_jammasuk_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_jammasuk_kerja`
--
ALTER TABLE `tb_jammasuk_kerja`
  ADD CONSTRAINT `tb_jammasuk_kerja_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `tb_pengajuan_cuti`
--
ALTER TABLE `tb_pengajuan_cuti`
  ADD CONSTRAINT `tb_pengajuan_cuti_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_pengajuan_izin`
--
ALTER TABLE `tb_pengajuan_izin`
  ADD CONSTRAINT `tb_pengajuan_izin_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD CONSTRAINT `tb_presensi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
