<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomController;

//Route::group( ['middleware' => 'guest'] , function() {
Route::get('/', 'IndexController@index');
Route::get('/post/{id}', 'IndexController@pengumuman');
Auth::routes();
Route::get('/log-in', function () {
    return view('auth.login');
});
Route::get('/presensi-berangkat', 'IndexController@presensiberangkat');
Route::get('/presensi-pulang', 'IndexController@presensipulang');
//Route::get('/cekdata', 'IndexController@cekdata');
Route::post('/simpan-presensi-berangkat', 'IndexController@simpanpresensiberangkat');
Route::post('/simpan-presensi-pulang/{id}', 'IndexController@simpanpresensipulang');
Route::get('/panduan', 'IndexController@panduan');
Route::post('/simpan-data-pegawai', 'IndexController@simpandatapegawai');
Route::post('/simpan-register', 'IndexController@simpanregister');
//Route::get('/input-izin', 'IndexController@inputizin');
Route::post('/simpan-izin-pegawai', 'IndexController@simpanizin');
Route::get('/data-pegawai', 'IndexController@datapegawai');
Route::get('/kehadiran-hari-ini', 'IndexController@kehadiran');
//});


//Route::get('/registrasi', 'AdminController@registrasi');
//Route::post('/simpan-registrasi', 'AdminController@simpanregistrasi');

Route::group(['middleware' => 'admin'], function () {
    Route::get('/dashboard', 'AdminController@dashboard');

    //Presensi
    Route::get('/presensi-harian', 'AdminController@presensi');
    Route::post('/presensi-cari-bulan', 'AdminController@caribulan');

    //Data presensi
    Route::get('/semua-presensi', 'AdminController@semuapresensi');
    Route::get('/laporan-presensi', 'AdminController@laporanpresensi');
    Route::get('/laporan', 'AdminController@laporan');
    Route::post('update-presensi/{id}', 'AdminController@updatepresensi');
    Route::get('downloadRekapKodeExcel/{dari}/{sampai}/{cluster}', 'AdminController@excelrekapkode');
    Route::get('/laporan-rekap', 'AdminController@laporanrekap');
    Route::get('downloadRekapJamExcel/{dari}/{sampai}/{cluster}', 'AdminController@excelrekapjam');

    //Lokasi Maps
    Route::get('/maps-location', 'AdminController@mapslocation');

    //Master cuti
    Route::get('/jenis-cuti', 'AdminController@jeniscuti');
    Route::post('/simpan-jenis-cuti', 'AdminController@simpanjeniscuti');
    Route::get('/{id}/delete-jenis-cuti', 'AdminController@deletejeniscuti');
    Route::post('update-jenis-cuti/{id}', 'AdminController@updatejeniscuti');
    Route::get('/manajemen-jatah-cuti', 'AdminController@manajemenjatahcuti');
    Route::post('/simpan-jatah-cuti', 'AdminController@simpanjatahcuti');
    Route::get('/{id}/delete-jatah-cuti', 'AdminController@deletejatahcuti');
    Route::post('update-jatah-cuti/{id}', 'AdminController@updatejatahcuti');

    //Cuti
    //Route::get('{id}/konfirmasi-cuti/', 'AdminController@konfirmasicuti');
    Route::get('/konfirmasi-cuti', 'AdminController@konfirmasicuti');
    Route::get('{id}/konfirmasi-tolak-cuti/', 'AdminController@tolakcuti');
    Route::get('{id}/konfirmasi-izinkan-cuti/', 'AdminController@izinkancuti');

    //Route::post('update-izin/{id}', 'AdminController@updateizin');
    //Route::get('/{id}/edit-izin', 'AdminController@editizin');
    //Route::post('/hapus-izin/{id}', 'AdminController@hapusizin');

    //Izin
    Route::get('/konfirmasi-izin', 'AdminController@konfirmasiizin');
    Route::get('{id}/konfirmasi-tolak-izin/', 'AdminController@tolakizin');
    Route::get('{id}/konfirmasi-izinkan-izin/', 'AdminController@izinkanizin');

    //Lembur
    Route::get('/konfirmasi-lembur', 'AdminController@konfirmasilembur');
    Route::get('{id}/konfirmasi-tolak-lembur/', 'AdminController@tolaklembur');
    Route::get('{id}/konfirmasi-izinkan-lembur/', 'AdminController@izinkanlembur');


    //Route::get('/jumlah-kehadiran', 'AdminController@jumlahkehadiran');

    Route::get('shift-kerja', 'App\Http\Controllers\AdminController@indexshift')->name('users.index');
    Route::post('users', 'App\Http\Controllers\UserController@update')->name('users.update');

    Route::get('/shift-kerja', 'AdminController@manajemenshift');
    Route::post('/simpan-shift-kerja', 'AdminController@simpanshift');
    Route::post('update-shift-kerja/{id}', 'AdminController@updateshift');
    Route::get('/{id}/delete-shift', 'AdminController@deleteshift');
    Route::get('/{id}/detail-shift', 'AdminController@detailshift');
    Route::post('/simpan-detail-shift', 'AdminController@simpandetailshift');
    Route::post('update-detail-shift/{id}', 'AdminController@updatedetailshift');
    Route::get('/{id}/delete-detail-shift', 'AdminController@deletedetailshift');
    Route::post('update-detail-masuk-kerja/{id}', 'AdminController@updatedetailmasukkerja'); 
    Route::post('edit-detail-masuk-kerja', 'AdminController@updatedetailmasukkerja1');    
    Route::get('/{id}/delete-detail-masuk-kerja', 'AdminController@deletedetailmasukkerja');
    

    Route::get('/jam-masuk', 'AdminController@jammasuk');
    Route::post('/simpan-jam', 'AdminController@simpanjam');
    Route::post('update-jam/{id}', 'AdminController@updatejam');
    Route::get('/{id}/edit-jam', 'AdminController@editjam');
    Route::post('/hapus-jam/{id}', 'AdminController@hapusjam');
    Route::get('/jam-masuk-kerja', 'AdminController@jammasukkerja');
    Route::post('/simpan-masuk-kerja', 'AdminController@simpanmasukkerja');
    Route::get('/{id}/detail-masuk-kerja', 'AdminController@detailmasukkerja');
    /* Route::get('/detail-masuk-kerja', 'CustomController@index'); */

    //Users
    Route::get('/manajemen-user', 'AdminController@manajemenuser');
    Route::get('/tambah-user', 'AdminController@tambahuser');
    Route::post('/simpan-user', 'AdminController@simpanuser');
    Route::get('/arsip-user', 'AdminController@arsipuser');

    //level
    Route::get('/level', 'CustomController@level');
    Route::post('/simpan-level', 'CustomController@simpanlevel');
    Route::post('update-level/{id}', 'CustomController@updatelevel');
    Route::get('/{id}/delete-level', 'CustomController@deletelevel');

    //cluster
    Route::get('/cluster', 'CustomController@cluster');
    Route::post('/simpan-cluster', 'CustomController@simpancluster');
    Route::post('update-cluster/{id}', 'CustomController@updatecluster');
    Route::get('/{id}/delete-cluster', 'CustomController@deletecluster');

    //departemen
    Route::get('/departemen', 'CustomController@departemen');
    Route::post('/simpan-departemen', 'CustomController@simpandepartemen');
    Route::post('update-departemen/{id}', 'CustomController@updatedepartemen');
    Route::get('/{id}/delete-departemen', 'CustomController@deletedepartemen');

    //jabatan
    Route::get('/jabatan', 'CustomController@jabatan');
    Route::post('/simpan-jabatan', 'CustomController@simpanjabatan');
    Route::post('update-jabatan/{id}', 'CustomController@updatejabatan');
    Route::get('/{id}/delete-jabatan', 'CustomController@deletejabatan');

    //Pengumuman
    Route::get('/manajemen-pengumuman', 'AdminController@manajemenpengumuman');
    Route::get('/{id}/delete-manajemen-pengumuman', 'AdminController@deletemanajemenpengumuman');
    Route::post('/update-manajemen-pengumuman/{id}', 'AdminController@updatemanajemenpengumuman');
    Route::post('/simpan-manajemen-pengumuman', 'AdminController@simpanmanajemenpengumuman');

    //Atur Header
    Route::get('/atur-header', 'AdminController@header');
    Route::post('/update-header/{id}', 'AdminController@updateheader');

    //Biodata
    Route::get('/biodata', 'AdminController@biodata');
    Route::get('/{id}/edit-biodata', 'AdminController@editbiodata');
    Route::get('/{id}/detail-biodata', 'AdminController@detailbiodata');
    Route::get('/{id}/edit-data', 'AdminController@editdata');
    Route::post('/update-data/{id}', 'AdminController@updatedata');
    Route::post('/hapus-data/{id}', 'AdminController@hapusdata');

    Route::get('/{id}/aktifkan', 'AdminController@aktifkan');

    //Radius
    Route::get('/atur-radius', 'AdminController@aturradius');
    Route::post('/update-radius/{id}', 'AdminController@updateradius');

    //Route untuk User biasa
    Route::post('/update-biodata', 'AdminController@updatebiodata');
    Route::get('/rekap-presensi', 'AdminController@rekappresensi');
    Route::get('/pengajuan-cuti', 'AdminController@pengajuancuti');
    Route::get('/daftar-cuti', 'AdminController@daftarcuti');
    Route::post('/simpan-pengajuan-cuti', 'AdminController@simpanpengajuancuti');
    Route::post('/batal-pengajuan-cuti/{id}', 'AdminController@batalpengajuancuti');
    Route::get('/daftar-izin', 'AdminController@daftarizin');
    Route::get('/pengajuan-izin', 'AdminController@pengajuanizin');
    Route::post('/simpan-pengajuan-izin', 'AdminController@simpanpengajuanizin');
    Route::post('/batal-pengajuan-izin/{id}', 'AdminController@batalpengajuanizin');
    Route::get('/jatah-cuti', 'AdminController@lihatcuti');
    /* Lembur */
    Route::get('/daftar-lembur', 'AdminController@daftarlembur');
    Route::get('/pengajuan-lembur', 'AdminController@pengajuanlembur');
    Route::post('/simpan-pengajuan-lembur', 'AdminController@simpanpengajuanlembur');
    Route::post('/batal-pengajuan-lembur/{id}', 'AdminController@batalpengajuanlembur');
});
