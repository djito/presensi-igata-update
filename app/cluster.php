<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cluster extends Model
{
    protected $table = 'cluster';
    protected $fillable = [
    'id',
    'nama_cluster',
    'keterangan'      
    ];	
}
