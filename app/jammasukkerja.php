<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jammasukkerja extends Model
{
    protected $table = 'tb_jammasuk_kerja';
    protected $fillable = [
    'id',
    'id_shift_kerja',
    'id_user',
    'id_shift',
    'tglmasuk',
    'jammasuk',
    'jampulang',
    'wf',
    'lembur',
    'lokasi',
    'id_atasan',
    'jml_jamkerja',
    'jml_lembur',
    ];	
}
