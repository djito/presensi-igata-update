<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shift extends Model
{
    protected $table = 'tb_shift_kerja';
    protected $fillable = [
    'id',
    'nama_shift',
    'jammasuk',
    'jampulang',
    'keterangan'  
    ];	
}
