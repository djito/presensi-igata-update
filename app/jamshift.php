<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jamshift extends Model
{
    protected $table = 'tb_jam_shift_kerja';
    protected $fillable = [
    'id',
    'id_shift_kerja	',
    'tglmasuk',
    'jammasuk',
    'jampulang',
    'wf',
    'lembur'  
    ];	
}
