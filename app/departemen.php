<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departemen extends Model
{
    protected $table = 'departemen';
    protected $fillable = [
    'id',
    'nama_dep',
    'keterangan'      
    ];	
}
