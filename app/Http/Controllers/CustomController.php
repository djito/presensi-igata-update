<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use App\level;
use datatables;
use App\cluster;
use App\jabatan;
use App\departemen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CustomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /* Level */
    public function level()
    {
        $data = DB::select('select * from level order by id ASC');
        $rec = count($data);

        echo "$rec" ;
        if ($rec == 0) {
            echo 'A';   
                DB::table('level')->insertOrIgnore([                    
                    ['id' => 1, 'nama_level' => 'Admin', 'keterangan' =>'Admin'],
                    ['id' => 3, 'nama_level' => 'Manajer', 'keterangan' =>'Manajer'],                    
                    ['id' => 6, 'nama_level' => 'Staff', 'keterangan' =>'Staff'],                    
                ]);
                $data = DB::select('select * from level order by id ASC');
                return view('admin.user.level', compact('data'));
        } else {
            echo '$rec'; 
            return view('admin.user.level', compact('data'));
        }

       /* foreach ($data as $key => $value) { 
            if ($value === null) {  
                echo 'A';    
                DB::table('level')->insertOrIgnore([
                    ['id' => 0, 'nama_level' => 'Direktur', 'keterangan' =>'Direktur'],
                    ['id' => 1, 'nama_level' => 'Admin', 'keterangan' =>'Admin'],
                    ['id' => 2, 'nama_level' => 'Manajer', 'keterangan' =>'Manajer'],
                    ['id' => 3, 'nama_level' => 'Supervisor', 'keterangan' =>'Supervisor'],
                    ['id' => 4, 'nama_level' => 'Koordinator', 'keterangan' =>'Koordinator'],
                    ['id' => 5, 'nama_level' => 'Staff', 'keterangan' =>'Staff'],                    
                ]);           
                return view('admin.user.level', compact('data'));
            } else {       
                echo 'B';
                return view('admin.user.level', compact('data'));
            }
        } */   
       /*  echo 'nothing';  */
        /* return view('admin.user.level', compact('data')); */
    }

    public function simpanlevel(Request $request)
    {
        $data = new level();
        $data->nama_level = $request->nama_level;        
        $data->keterangan = $request->keterangan;
        $data->save();
        Session::flash('sukses', 'Data berhasil disimpan');
        return back();
    }

    public function updatelevel(Request $request, $id)
    {
        $data = level::find($id);
        $data->nama_level = $request->nama_level; 
        $data->keterangan = $_POST['keterangan'];
        $data->save();
        Session::flash('sukses', 'Data berhasil diupdate');
        return back();
    }

    public function deletelevel(Request $request, $id)
    {
        DB::delete('delete from level where id = "' . $id . '"');

        /* return redirect('') 
        ->with(Session::flash('sukses', 'Data berhasil dihapus'))
        ->withErrors(array('message' => 'Fail!'));
        return back(); */
        
       Session::flash('sukses', 'Data berhasil dihapus');
        return back(); 
    } 

    /* cluster */
    public function cluster()
    {
        $data = DB::select('select * from cluster order by id ASC');
        
        return view('admin.user.cluster', compact('data'));
    }

    public function simpancluster(Request $request)
    {
        $data = new cluster();
        $data->nama_cluster = $request->nama_cluster;        
        $data->keterangan = $request->keterangan;
        $data->save();
        Session::flash('sukses', 'Data berhasil disimpan');
        return back();
    }

    public function updatecluster(Request $request, $id)
    {
        $data = cluster::find($id);
        $data->nama_cluster = $request->nama_cluster; 
        $data->keterangan = $_POST['keterangan'];
        $data->save();
        Session::flash('sukses', 'Data berhasil diupdate');
        return back();
    }

    public function deletecluster(Request $request, $id)
    {
        DB::delete('delete from cluster where id = "' . $id . '"');
        Session::flash('sukses', 'Data berhasil dihapus');
        return back();
    }

    /* departemen */
    public function departemen()
    {
        $data = DB::select('select * from departemen order by id ASC');
        return view('admin.user.departemen', compact('data'));
    }

    public function simpandepartemen(Request $request)
    {
        $data = new departemen();
        $data->nama_dep = $request->nama_dep;        
        $data->keterangan = $request->keterangan;
        $data->save();
        Session::flash('sukses', 'Data berhasil disimpan');
        return back();
    }

    public function updatedepartemen(Request $request, $id)
    {
        $data = departemen::find($id);
        $data->nama_dep = $request->nama_dep; 
        $data->keterangan = $_POST['keterangan'];
        $data->save();
        Session::flash('sukses', 'Data berhasil diupdate');
        return back();
    }

    public function deletedepartemen(Request $request, $id)
    {
        DB::delete('delete from departemen where id = "' . $id . '"');
        Session::flash('sukses', 'Data berhasil dihapus');
        return back();
    }

    /* jabatan */
    public function jabatan()
    {
        $data = DB::select('select * from jabatan order by id ASC');
        
        return view('admin.user.jabatan', compact('data'));
    }

    public function simpanjabatan(Request $request)
    {
        $data = new jabatan();
        $data->nama_jabatan = $request->nama_jabatan;        
        $data->keterangan = $request->keterangan;
        $data->save();
        Session::flash('sukses', 'Data berhasil disimpan');
        return back();
    }

    public function updatejabatan(Request $request, $id)
    {
        $data = jabatan::find($id);
        $data->nama_jabatan = $request->nama_jabatan; 
        $data->keterangan = $_POST['keterangan'];
        $data->save();
        Session::flash('sukses', 'Data berhasil diupdate');
        return back();
    }

    public function deletejabatan(Request $request, $id)
    {
        DB::delete('delete from jabatan where id = "' . $id . '"');
        Session::flash('sukses', 'Data berhasil dihapus');
        return back();
    }

}
