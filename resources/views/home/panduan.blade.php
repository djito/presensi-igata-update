@extends('layouts.app')
@section('content')
<div class="py-3">
    <div class="row">
        <div class="col-md-8">
            <h2>Panduan Presensi Online</h2>
            <h3 class="mb-3">Pengumuman</h3>
            <?php

            use Illuminate\Support\Str; ?>
            @foreach($data as $row)
            <div class="card card-body mb-3">
                <!--<img src="{{asset('img/pengumuman')}}/{{$row->gambar}}" class="img-fluid" alt="{{$row->judul}}" title="{{$row->judul}}" />-->
                <small class="mb-3"><i class="fas fa-calendar-day"></i> {{date_format(date_create($row->tanggal),"d/m/Y")}} - Oleh {{$row->nama}}</small>
                <h3>
                    <a href="{!! url('/post/'.$row->url.'') !!}" class="text-decoration-none" title="{{$row->judul}}">{{$row->judul}}</a>
                </h3>
                {!! Str::limit($row->isi, 150, ' ...') !!} <a href="{!! url('/post/'.$row->url.'') !!}" class="text-decoration-none" title="{{$row->judul}}">Selengkapnya</a>
            </div>
            @endforeach
        </div>
        <!-- <div class="card card-body">
                <p>
                    Presensi online merupakan aplikasi yang dibangun untuk melakukan monitoring kinerja pegawai</p>
                Adapun cara melakukan presensi online adalah sebagai berikut:
                <ul>
                    <li><strong>Langkah Awal</strong></li>
                    1.) Ketik alamat http://localhost/presensi<br />
                    2.) Pilih Menu Home<br />
                    <li><strong>Presensi Berangkat</strong></li>
                    <img src="{{asset('img/panduan/Snapshot_2020-12-28_213729_localhost.png')}}" width="100%"><br /><br />
                    <div class="alert alert-light">
                        1.) Pilih tombol <strong>Berangkat</strong><br />
                        2.) Masukan NIK <br />
                        3.) Klik tombol <strong>Simpan</strong>
                    </div><br />
                    <li><strong>Presensi Berangkat (Selanjutnya)</strong></li>
                    <img src="{{asset('img/panduan/Snapshot_2020-12-28_213947_localhost.png')}}" width="100%"><br /><br />
                    <div class="alert alert-light">
                        Klik tombol <strong>simpan presensi</strong><br />
                    </div>
                    <li><strong>Presensi Pulang</strong></li>
                    <img src="{{asset('img/panduan/Snapshot_2020-12-28_214014_localhost.png')}}" width="100%"><br /><br />
                    <div class="alert alert-light">
                        1.) Pilih tombol <strong>Pulang</strong><br />
                        2.) Masukan NIK <br />
                        3.) Klik tombol <strong>Simpan</strong>
                        <br /><br />
                        <img src="{{asset('img/panduan/Snapshot_2020-12-28_214710_localhost.png')}}" width="100%"><br /><br />
                </ul>

            </div> -->
    </div>

    <div class="col-md-4">

    </div>
</div>
@endsection