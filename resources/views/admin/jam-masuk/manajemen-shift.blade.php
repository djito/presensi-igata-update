@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Shift Kerja</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Shift Kerja</li>
</ol>
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-12">   
            <div class="card-header">         
                {{-- Tambah Shift --}}
                {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
                
                {{-- Row 1 --}}
                <div class="row" style="max-width: 100%;">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Nama Shift</label>                                      
                            <div class="form-group">   
                                {{-- <input id="nama_shift" type="text" class="form-control" name="nama_shift" value="" placeholder="Nama Shift" required autofocus> --}}
                                <select name="nama_shift" id="nama_shift" class="form-select">
                                    <option value="">- Pilih -</option>
                                    <option value="PAGI">PAGI</option>
                                    <option value="SIANG">SIANG</option>
                                    <option value="MALAM">MALAM</option>
                                    <option value="OFF">OFF</option>
                                </select>
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Jam  Masuk</label>                                      
                            <div class="form-group"> 
                                    <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="00:00" required autofocus >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Jam  Pulang</label>                                      
                            <div class="form-group"> 
                                    <input id="jampulang" type="text" class="form-control" name="jampulang" value="00:00" required autofocus >
                            </div>                                      
                        </div> 
                    </div>    
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>{{ __('Keterangan *') }}</label>                                      
                            <div class="form-group"> 
                                <input class="form-control" name="keterangan"  value="" required placeholder="Keterangan" required autofocus>
                            </div>                                      
                        </div> 
                    </div>  
                    
                    <div class="col-sm-2 mt-4" >
                        <div class="form-group">                            
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-plus"></i> Tambah
                            </button>                                     
                        </div> 
                    </div>   
                    
                </div>
    
                {{-- <div class="row" style="max-width: 50%;">                        
                             
                </div>    --}} 
                
                {!! Form::close() !!}
                {{-- Card header end --}}
            </div>
             {{-- Tambah Shift End --}}
            
            
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-bordered {{-- table-striped --}} table-bordered table-hover" id="dataTable" width="800%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center" style="max-width: 3px;">NO</th>
                                <th class="text-left" width="20%">Shift Kerja</th>
                                <th class="text-left" width="20%">Jam Masuk</th>
                                <th class="text-left" width="20%">Jam Pulang</th>
                                <th class="text-left">Keterangan</th>
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($data as $key => $value)
                            <tr>
                                <td align="text-center" style="max-width: 3px;">{{$no}}</td>
                                <td>{!!$value->nama_shift!!}</td>
                                <td>{!!$value->jammasuk!!}</td>
                                <td>{!!$value->jampulang!!}</td>
                                <td>{!!$value->keterangan!!}</td>
                                <td align="center">                               
                                    
                                    <button title="Edit" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal{{$value->id}}"> <i class="fa fa-edit"></i></button>
                                    <a title="Hapus" href="#" type="button" class="btn btn-danger btn-sm tooltipku" data-bs-toggle="modal" data-bs-target="#myModal1{{$value->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>

{{-- Modal edit shift --}}
@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-shift-kerja', $row->id]]) !!}
<div class="modal fade" id="largeModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">    
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nama Shift</label>                                      
                        <div class="form-group">   
                            <input id="nama_shift" type="text" class="form-control" name="nama_shift" value="{{ $row->nama_shift}}" required autofocus>
                        </div>                                      
                    </div> 
                </div>
                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jam  Masuk</label>                                      
                        <div class="form-group"> 
                                <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="{{$row->jammasuk}}" required autofocus >
                        </div>                                      
                    </div> 
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jam  Pulang</label>                                      
                        <div class="form-group"> 
                                <input id="jampulang" type="text" class="form-control" name="jampulang" value="{{$row->jampulang}}" required autofocus >
                        </div>                                      
                    </div> 
                </div>                   
            </div>

            <div class="row">
                <div class="col-sm-8 mt-2">
                    <div class="form-group">
                        <label>{{ __('Keterangan *') }}</label>                                      
                        <div class="form-group"> 
                            <input class="form-control" name="keterangan"  value="{{$row->keterangan}}" required>
                        </div>                                      
                    </div> 
                </div>                  
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach

{{-- Modal Hapus shift --}}
@foreach($data as $row)
<div class="modal fade" id="myModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Data ?</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus Data?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <a title="Hapus" href="{!! url('/'.$row->id.'/delete-shift') !!}" class="btn btn-danger" ><i class="fa fa-trash"></i> Ok</a>
        </div>
    </div>
</div>
</div>
@endforeach

@endsection