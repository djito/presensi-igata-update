@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Detail Shift Kerja</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('/shift-kerja')}}"> Shift Kerja</a></li>
</ol>
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-12">  
            <div class="card-header">
                {!! Form::open(['url' => 'simpan-detail-shift']) !!}
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">                                                                
                            <div class="form-group"> 
                                    <input id="nama_shift" type="text" class="form-control" name="nama_shift" value="{{$nama_shift}}" disabled >
                                    <select name="nama_shift" id="nama_shift" class="form-select">
                                        <option value="">- Pilih -</option>
                                        <option value="1">Pagi</option>
                                        <option value="2">Siang</option>
                                        <option value="3">Malam</option>
                                        <option value="0">Off</option>
                                    </select>
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">                                                                
                            <div class="form-group"> 
                                    <input id="idshift" type="text" class="form-control" name="idshift" value="{{$idshift}}" required autofocus hidden>
                            </div>                                      
                        </div> 
                    </div>

                </div>
                        

             {{-- Row 1 --}}
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="tglmasuk" value="{{ old('tanggal') }}">
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Masuk</label>                                      
                            <div class="form-group"> 
                                    <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="00:00" required autofocus >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Pulang</label>                                      
                            <div class="form-group"> 
                                    <input id="jampulang" type="text" class="form-control" name="jampulang" value="00:00" required autofocus >
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>WFH/WFO/OFF</label>                                       
                            <select class="form-control" id="wf" name="wf">
                                <option value="">Pilih</option>
                                <option value="WFH" >WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select>  
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Tambah
                </button>  
                {!! Form::close() !!}
            </div>

            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-bordered {{-- table-striped --}} table-bordered table-hover" id="dataTable" width="800%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">NO</th>
                                <th class="text-left">Tanggal</th>
                                <th class="text-left">Jam Masuk</th>
                                <th class="text-left">Jam Pulang</th>
                                <th class="text-left">WFH/WFO/OFF</th>
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($data as $key => $value)
                            <tr>
                                <td align="center">{{$no}}</td>
                                <td>{!!$value->tglmasuk!!}</td>
                                <td>{!!$value->jammasuk!!}</td>
                                <td>{!!$value->jampulang!!}</td>
                                <td>{!!$value->wf!!}</td>
                                {{-- <td align="center">
                                    <a href="">
                                        <input type="button" class="btn btn-outline-primary btn-sm" value="Lihat" data-bs-toggle="modal" data-bs-target="#largeModal1{{$value->id}}">
                                    </a>
                                    <button class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal1{{$value->id}}">Lihat</button>
                                </td> --}}
                                <td align="center">
                                    <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal1{{$value->id}}"> <i class="fa fa-edit"></i></button>
                                    <a title="Hapus" href="#" type="button" class="btn btn-danger btn-sm tooltipku" data-bs-toggle="modal" data-bs-target="#myModal1{{$value->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                                </tr>
                                <?php $no++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
</div>

@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-detail-shift', $row->id]]) !!}
<div class="modal fade" id="largeModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
       
            <div class="card-body">
        
                {{-- Row 1 --}}
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="tglmasuk" value="{{ $row->tglmasuk}}" disabled>
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Masuk</label>                                      
                            <div class="form-group"> 
                                    <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="{{ $row->jammasuk}}" required autofocus >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Pulang</label>                                      
                            <div class="form-group"> 
                                    <input id="jampulang" type="text" class="form-control" name="jampulang" value="{{ $row->jampulang}}" required autofocus >
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>WFH/WFO/OFF</label> 
                            @if($row->wf == 'WFO')                                      
                            <select class="form-control" id="wf" name="wf" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH">WFH</option>
                                <option value="WFO" selected="">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select>  
                            @elseif($row->wf == 'WFH')
                            <select class="form-control" id="wf" name="wf" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH" selected="">WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select> 
                            @else
                            <select class="form-control" id="wf" name="wf" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH">WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF" selected="">OFF</option>                                                                                                    
                            </select> 
                            @endif
                        </div>
                    </div>

                </div>
                   
                
            </div>
            <!-- /.card-body -->

            {{-- END TAMBAH DATA USER --}}
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach

@foreach($data as $row)
<div class="modal fade" id="myModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Data ?</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus Data?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <a title="Hapus" href="{!! url('/'.$row->id.'/delete-detail-shift') !!}" class="btn btn-danger" ><i class="fa fa-trash"></i> Ok</a>
        </div>
    </div>
</div>
</div>
@endforeach


<script>
    $(function () {
        $('#datetimepicker1').datetimepicker();
        //Date picker
    $('#reservationdate').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#tabel2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "scrollX": true,
        /* "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"] */
        })

    
    })
</script>


@endsection