@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Detail Jam Masuk Kerja</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{url('/jam-masuk-kerja')}}"> Jam Masuk Kerja</a></li>
</ol>
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-12">    
            
            <div class="card-header">
                {!! Form::open(['url' => 'simpan-masuk-kerja']) !!}
                <div class="row">
                    
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->                        
                        <div class="form-group">   
                            <label>Nama</label>
                            <input name="id_user" id="id_user" value="{{$nik}}" hidden>
                            <input id="nama_shift" type="text" class="form-control" name="nama_shift" value="{{$nik}}-{{$nama}}" disabled > 
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">                            
                            <label>Shift Kerja</label>                             
                            <select name="id_shift" id="id_shift" class="form-select" required >
                                <option value="">- Pilih -</option>
                                {{-- <option value="OFF" 
                                    data-jammasuk="00:00:00"
                                    data-jampulang="00:00:00"
                                    >OFF</option> --}}
                                @foreach($shift as $row)
                                <option value="{{$row->id}}" 
                                    data-jammasuk="<?=$row->jammasuk?>"
                                    data-jampulang="<?=$row->jampulang?>"                                    
                                    >{{$row->id}}-{{$row->nama_shift}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>       
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Jam  Masuk</label>                                      
                            <div class="form-group"> 
                                    <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="00:00:00" disabled required >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Jam  Pulang</label>                                      
                            <div class="form-group"> 
                                    <input id="jampulang" type="text" class="form-control" name="jampulang" value="00:00:00" disabled required  >
                            </div>                                      
                        </div> 
                    </div>    
                                                  

                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="tglmasuk" value="{{old('tanggal') }}" required >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>WFH/WFO/OFF</label>                                                                
                            <select class="form-control" id="wf" name="wf" required {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH">WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select>                             
                        </div>
                    </div>

                    <div class="col-sm-2 mt-4"> 
                        <div class="form-group">                       
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button>  
                        </div>                      
                    </div> 
                </div>
                {!! Form::close() !!}                 
                
            </div>

            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-bordered table-striped  table-bordered table-hover" id="dataTable" width="800%" cellspacing="0">
                    {{-- <table id="tabel1" class="uk-table uk-table-hover uk-table-striped uk-text-small uk-text-nowrap" style="width: 100%"> --}}
                        <thead>
                            <tr>
                                <th class="text-center">NO</th>
                                <th class="text-left">Tanggal</th>
                                <th class="text-left">Shift</th>
                                <th class="text-left">Jam Masuk</th>
                                <th class="text-left">Jam Pulang</th>
                                <th class="text-left">WFH/WFO/OFF</th>
                                <th class="text-left">lokasi</th>
                                <th class="text-left">Lembur</th>                                
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                       {{-- tbody --}}
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
</div>

@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-detail-masuk-kerja', $row->id]]) !!}
<div class="modal fade" id="largeModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
       
            <div class="card-body">
        
                {{-- Row 1 --}}
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" id="id1{{$row->id}}" name="id1{{$row->id}}" value="{{ $row->id}}" hidden>
                                <input type="date" class="form-control" id="tglmasuk1{{$row->id}}" name="tglmasuk1{{$row->id}}" value="{{ $row->tglmasuk}}" disabled>
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">                            
                            <label>Shift Kerja</label>                             
                            <select name="id_shift1{{$row->id}}" id="id_shift1{{$row->id}}" class="form-select" required >
                                <option value="">- Pilih -</option>
                                @foreach($shift1 as $val)
                                <option value="{{$val->id}}" 
                                    data-jammasuk1{{$row->id}}="<?=$val->jammasuk?>"
                                    data-jampulang1{{$row->id}}="<?=$val->jampulang?>"
                                    <?php if ($row->id_shift_kerja == $val->id ) echo 'selected'?>
                                    
                                    >{{$val->id}}-{{$val->nama_shift}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>    
                    
                    <div class="col-sm-2">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Masuk</label>                                      
                            <div class="form-group"> 
                                    <input id="jammasuk1{{$row->id}}" type="text" class="form-control" name="jammasuk1{{$row->id}}" value="{{ $row->jammasuk}}" required disabled >
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Jam  Pulang</label>                                      
                            <div class="form-group"> 
                                    <input id="jampulang1{{$row->id}}" type="text" class="form-control" name="jampulang1{{$row->id}}" value="{{ $row->jampulang}}" required disabled >
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>WFH/WFO/OFF</label> 
                            @if($row->wf == 'WFO')                                      
                            <select class="form-control" id="wf1{{$row->id}}" name="wf{{$row->id}}" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH">WFH</option>
                                <option value="WFO" selected="">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select>  
                            @elseif($row->wf == 'WFH')
                            <select class="form-control" id="wf1{{$row->id}}" name="wf{{$row->id}}" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH" selected="">WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select> 
                            @else
                            <select class="form-control" id="wf1{{$row->id}}" name="wf{{$row->id}}" {{-- value="{{ $row->wf}}" --}}>
                                <option value="">Pilih</option>
                                <option value="WFH">WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF" selected="">OFF</option>                                                                                                    
                            </select> 
                            @endif
                        </div>
                    </div>

                </div>
                   
                
            </div>
            <!-- /.card-body -->

            {{-- END TAMBAH DATA USER --}}
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach

@foreach($data as $row)
<div class="modal fade" id="myModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Data ?</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus Data?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <a title="Hapus" href="{!! url('/'.$row->id.'/delete-detail-masuk-kerja') !!}" class="btn btn-danger" ><i class="fa fa-trash"></i> Ok</a>
        </div>
    </div>
</div>
</div>
@endforeach


<script>
    $(function () {
        $('#datetimepicker1').datetimepicker();
        //Date picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY/MM/DD'
    });

    $("#tabel2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "scrollX": true,
        /* "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"] */
        })

    
    });

    $(document).ready(function () {
        loaddata()
    })

    function loaddata() {
        $('#tabel1').DataTable({
            scrollX: true,
            serverSide: true,
            processing: true,
            ajax:{
                url: "{{route('/detail-masuk-kerja')}}"                    
                 },
            columns: [          
                {data: 'nama_ref', name: 'nama_ref'},
                {data: 'komunitas', name: 'komunitas'},
                {data: 'tgl', name: 'tgl'},                
                {data: 'rt', name: 'rt'},
                {data: 'rw', name: 'rw'},
                {data: 'kelurahan', name: 'kelurahan'},                
                {data: 'pj', name: 'pj'},
                {data: 'no_hp', name: 'no_hp'},               
                {/* data: 'status', name: 'status' */
                data: null,
                        render: function (data) {
                            if(data.status=="Pending") {
                                return "<div class = 'badge badge-warning'>" + data.status + "</div>";
                            } else if (data.status=="Approved") {
                                return "<div class = 'badge badge-success'>" + data.status + "</div>";
                            } else {
                                return "<div class = 'badge badge-danger'>" + data.status + "</div>";
                            }                            
                        }
            },                
                {data: 'aksi', name: 'aksi', orderable: false},
            ]
        });
    }


    $(document).ready(function () {
        loaddata();
        $(document).on('click', '#pilih', function() {                
                var id = $(this).data('id');
                var id_shift = $(this).data('id_shift_kerja');
                var tglmasuk = $(this).data('tglmasuk');
                var jammasuk = $(this).data('jammasuk');
                var jampulang = $(this).data('jampulang');
                var wf = $(this).data('wf');                   
                
                $('#id1').val(id);
                $('#id_shift1').val(id_shift);
                $('#tglmasuk1').val(tglmasuk)  ;              
                $('#jammasuk1').val(jammasuk);
                $('#jampulang1').val(jampulang);
                $('#wf1').val(wf);           
               
            });
    })

    
    $(document).ready(function() {
    $('#id_shift').on('change', function() {
        const selected = $(this).find('option:selected');        
        const jammasuk = selected.data('jammasuk');
        const jampulang = selected.data('jampulang');    

        $('#jammasuk').val(jammasuk);
        $('#jampulang').val(jampulang);
         

    });

    $('#id_shift1').on('change', function() {        
        const selected = $(this).find('option:selected');        
        var jammasuk1 = selected.data('jammasuk1');
        var jampulang1 = selected.data('jampulang1');   
        
        var id = $(this).data('id');
        var id_shift = $(this).data('id_shift_kerja');
        var tglmasuk = $(this).data('tglmasuk');
        var jammasuk = $(this).data('jammasuk');
        var jampulang = $(this).data('jampulang');
        var wf = $(this).data('wf');     

        $('#jammasuk1').val(jammasuk1);
        $('#jampulang1').val(jampulang1);
        $('#id1').val(id);
        $('#id_shift1').val(id_shift);
        $('#tglmasuk1').val(tglmasuk)  ;              
        $('#jammasuk1').val(jammasuk);
        $('#jampulang1').val(jampulang);
        $('#wf1').val(wf);

    });

    });

</script>


@endsection