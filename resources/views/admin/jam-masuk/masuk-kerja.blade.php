@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Jam Masuk Kerja</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Jam Masuk Kerja</li>
</ol>
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-12">
            <div class="card-header">
               {{--  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
                 <i class="fa fa-plus"></i> Tambah
                </button> --}}
                {{-- Carh Heeader --}}
            </div>                
              
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-bordered table-bordered table-hover" id="dataTable" width="60%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">NO</th>
                                <th class="text-left" width="20%">Nama Karyawan</th>
                                {{-- <th class="text-left" width="30%">Shift Kerja</th> --}}
                                <th class="text-center" width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($data as $key => $value)
                            <tr>
                                <td align="center">{{$no}}</td>
                                <td>{!!$value->nik!!}-{!!$value->nama!!}</td>
                                {{-- <td>{!!$value->nama_shift!!}</td> --}}
                                <td align="center">
                                    <a href="{!! url('/'.$value->id_user.'/detail-masuk-kerja') !!}" title="Detail Jam Kerja">
                                        <input type="button" class="btn btn-outline-primary btn-sm" value="Lihat" >
                                    </a>
                                    {{-- <button class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal1{{$value->id}}">Lihat</button> --}}
                                    {{--  </td>
                                    <td align="center"> --}}
                                    {{-- <button title="Edit" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal{{$value->id}}"> <i class="fa fa-edit"></i></button>
                                    <a title="Hapus" href="#" type="button" class="btn btn-danger btn-sm tooltipku" data-bs-toggle="modal" data-bs-target="#myModal1{{$value->id}}"><i class="fa fa-trash"></i></a> --}}
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>
    </div>
</div>


<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document" >
  {!! Form::open(['url' => 'simpan-masuk-kerja']) !!}
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Shift Kerja</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">

        <div class="form-group row mb-3">
            <label for="nama_shift" class="col-md-3 col-form-label">{{ __('Nama Karyawan *') }}</label>
            <div class="col-md-6">
                <select name="id_user" id="id_user" class="form-select">
                    <option value="">- Pilih -</option>
                    @foreach($nama as $row)
                    <option value="{{$row->id}}">{{$row->id}}-{{$row->nama}}</option>
                    @endforeach
                  </select>
                {{-- <input id="nama_shift" type="text" class="form-control{{ $errors->has('nama_shift') ? ' is-invalid' : '' }}" name="nama_shift" value="{{ $value->nama_shift}}" required autofocus>
 --}}
              @if ($errors->has('nama_shift'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('nama_shift') }}</strong>
              </span>
              @endif
            </div>
        </div>
        <div class="form-group row mb-3">
        <label for="nama_shift" class="col-md-3 col-form-label">{{ __('Shift Kerja *') }}</label>
            <div class="col-md-4">
                <select name="id_shift" id="id_shift" class="form-select">
                    <option value="">- Pilih -</option>
                    @foreach($shift as $row)
                    <option value="{{$row->id}}">{{$row->id}}-{{$row->nama_shift}}</option>
                    @endforeach
                  </select>
              {{-- <input class="form-control" name="keterangan"  value="{{$value->id}}" required> --}}
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>


@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-shift-kerja', $row->id]]) !!}
<div class="modal fade" id="largeModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
           
            <div class="form-group row mb-3">
                <label for="nama_shift" class="col-md-2 col-form-label">{{ __('Shift Kerja *') }}</label>
                <div class="col-md-10">
                    <input id="nama_shift" type="text" class="form-control{{ $errors->has('nama_shift') ? ' is-invalid' : '' }}" name="nama_shift" value="{{ $row->id_shift}}" required autofocus>

                  @if ($errors->has('nama_shift'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('nama_shift') }}</strong>
                </span>
                @endif
                </div>
            </div>
            <div class="form-group row mb-3">
            <label for="nama_shift" class="col-md-2 col-form-label">{{ __('Keterangan *') }}</label>
                <div class="col-md-10">
                  <input class="form-control" name="keterangan"  value="{{$row->id}}" required>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach

@foreach($data as $row)
<div class="modal fade" id="myModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Data ?</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus Data?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <a title="Hapus" href="{!! url('/'.$row->id.'/delete-shift') !!}" class="btn btn-danger" ><i class="fa fa-trash"></i> Ok</a>
        </div>
    </div>
</div>
</div>
@endforeach

@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-shift-kerja', $row->id]]) !!}
<div class="modal fade" id="largeModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
           
            
       
            <div class="card-body">
        
                {{-- Row 1 --}}
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal Lahir</label>                                      
                            <div class="form-group">                                            
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal Lahir</label>                                      
                            <div class="form-group">                                            
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>                                      
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Tanggal Lahir</label>                                      
                            <div class="form-group">                                            
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>                                      
                        </div> 
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>WFH/WFO/OFF</label>                                       
                            <select class="form-control" id="wf" name="wf">
                                <option value="">Pilih</option>
                                <option value="WFH" >WFH</option>
                                <option value="WFO">WFO</option>   
                                <option value="OFF">OFF</option>                                                                                                    
                            </select>  
                        </div>
                    </div>
                </div>
                {{-- EndRow 1 --}}
    
                {{-- Row 2 --}}
                
                   
                
            </div>
            <!-- /.card-body -->

            {{-- END TAMBAH DATA USER --}}
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach




@endsection