@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Pengajuan Lembur</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Pengajuan Lembur</li>
</ol>
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-12">   
            <div class="card-header">    
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModal"><i class="fa fa-plus"></i> Tambah
                </button>                                    
            </div> 
        </div>
            
            
        <div class="card-body">
            <div class="table table-responsive">
                <table class="table table-bordered {{-- table-striped --}} table-bordered table-hover" id="dataTable" width="800%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-center" style="max-width: 3px;">NO</th>
                            <th class="text-left" width="20%">Shift Kerja</th>
                            <th class="text-left" width="20%">Jam Masuk</th>
                            <th class="text-left" width="20%">Jam Pulang</th>
                            <th class="text-left">Keterangan</th>
                            <th class="text-center" width="10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                       {{--  @foreach($data as $key => $value)
                        <tr>
                            <td align="text-center" style="max-width: 3px;">{{$no}}</td>
                            <td>{!!$value->nama_shift!!}</td>
                            <td>{!!$value->jammasuk!!}</td>
                            <td>{!!$value->jampulang!!}</td>
                            <td>{!!$value->keterangan!!}</td>
                            <td align="center">                               
                                
                                <button title="Edit" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#largeModal{{$value->id}}"> <i class="fa fa-edit"></i></button>
                                <a title="Hapus" href="#" type="button" class="btn btn-danger btn-sm tooltipku" data-bs-toggle="modal" data-bs-target="#myModal1{{$value->id}}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach --}}
                    </tbody>
                </table>
                
            </div>
        </div>
        
    </div>    
</div>

{{-- TAMBAH PENGAJUAN LEMBUR --}}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" >
        {!! Form::open(['url' => 'simpan-lembur']) !!}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Jatah Cuti</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group row mb-3">
                    <label for="nama" class="col-md-4 col-form-label">{{ __('Nama *') }}</label>
                    <div class="col-md-8">
                    <select name="id_user" class="form-control" data-live-search="true">
                        <option value="" data-tokens="mustard">- Pilih -</option>
                        {{-- @foreach($nama as $row)
                        <option value="{{$row->id}}" data-tokens="mustard">{{$row->nama}}</option>
                        @endforeach --}}
                    </select>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="cuti_tahunan" class="col-md-4 col-form-label">{{ __('Cuti Tahunan *') }}</label>
                    <div class="col-md-3">
                    <input id="cuti_tahunan" type="number" class="form-control{{ $errors->has('cuti_tahunan') ? ' is-invalid' : '' }}" name="cuti_tahunan" value="{{ old('cuti_tahunan') }}" required autofocus>

                    @if ($errors->has('cuti_tahunan'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('cuti_tahunan') }}</strong>
                    </span>
                    @endif
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="cuti_bersama" class="col-md-4 col-form-label">{{ __('Cuti bersama *') }}</label>
                    <div class="col-md-3">
                    <input id="cuti_bersama" type="number" class="form-control{{ $errors->has('cuti_bersama') ? ' is-invalid' : '' }}" name="cuti_bersama" value="{{ old('cuti_bersama') }}" required autofocus>

                    @if ($errors->has('cuti_bersama'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('cuti_bersama') }}</strong>
                    </span>
                    @endif
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="cuti_berjalan" class="col-md-4 col-form-label">{{ __('Cuti Berjalan *') }}</label>
                    <div class="col-md-3">
                        <input id="cuti_berjalan" type="number" value="0" class="form-control{{ $errors->has('cuti_berjalan') ? ' is-invalid' : '' }}" name="cuti_berjalan" required autofocus>

                        @if ($errors->has('cuti_berjalan'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('cuti_berjalan') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="cuti_lain" class="col-md-4 col-form-label">{{ __('Cuti Lain *') }}</label>
                    <div class="col-md-3">
                    <input id="cuti_lain" type="number" value="0" class="form-control{{ $errors->has('cuti_lain') ? ' is-invalid' : '' }}" name="cuti_lain" required autofocus>

                    @if ($errors->has('cuti_lain'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('cuti_lain') }}</strong>
                    </span>
                    @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>      
</div>


{{-- Modal edit shift --}}
@foreach ($data as $row)    
{!! Form::model($row, ['url' => ['/update-shift-kerja', $row->id]]) !!}
<div class="modal fade" id="largeModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    {!! Form::open(['url' => 'simpan-shift-kerja']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Edit Shift Kerja</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">    
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nama Shift</label>                                      
                        <div class="form-group">   
                            <input id="nama_shift" type="text" class="form-control" name="nama_shift" value="{{ $row->nama_shift}}" required autofocus>
                        </div>                                      
                    </div> 
                </div>
                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jam  Masuk</label>                                      
                        <div class="form-group"> 
                                <input id="jammasuk" type="text" class="form-control" name="jammasuk" value="{{$row->jammasuk}}" required autofocus >
                        </div>                                      
                    </div> 
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Jam  Pulang</label>                                      
                        <div class="form-group"> 
                                <input id="jampulang" type="text" class="form-control" name="jampulang" value="{{$row->jampulang}}" required autofocus >
                        </div>                                      
                    </div> 
                </div>                   
            </div>

            <div class="row">
                <div class="col-sm-8 mt-2">
                    <div class="form-group">
                        <label>{{ __('Keterangan *') }}</label>                                      
                        <div class="form-group"> 
                            <input class="form-control" name="keterangan"  value="{{$row->keterangan}}" required>
                        </div>                                      
                    </div> 
                </div>                  
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>      
{!! Form::close() !!}
</div>
</div>
@endforeach

{{-- Modal Hapus shift --}}
@foreach($data as $row)
<div class="modal fade" id="myModal1{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Hapus Data ?</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus Data?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
            <a title="Hapus" href="{!! url('/'.$row->id.'/delete-shift') !!}" class="btn btn-danger" ><i class="fa fa-trash"></i> Ok</a>
        </div>
    </div>
</div>
</div>
@endforeach

@endsection