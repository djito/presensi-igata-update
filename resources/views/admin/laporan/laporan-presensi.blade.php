@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Laporan Presensi</h2>
<ol class="breadcrumb mb-3">
  <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
  <li class="breadcrumb-item active">Laporan Presensi</li>
</ol>

<div class="card">
  <div class="card-body">
    <div class="alert alert-warning text-dark">
      <small><i class="fas fa-fw fa-info-circle"></i> Cari laporan presensi berdasarkan rentang tanggal</small>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="card card-body">
          <h5 class="mb-3">Laporan Rekap Kode</h5>
          <form action="{{ url('/laporan') }}" method="GET" target="_blank">
            <div class="mb-3">
              <label for="" class="form-label">Tanggal Mulai</label>
              <input type="date" class="form-control" name="dari" value="{{ old('tanggal') }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Tanggal Akhir</label>
              <input type="date" class="form-control" name="sampai" value="{{ old('tanggal') }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Cluster</label>
              <select type="text" class="form-select" name="cluster">
                <option value="">- Pilih -</option>
                <option value="Tetap">Tetap</option>
                <option value="Honorer">Honorer</option>
                <option value="Satpam">Satpam</option>
                <option value="OB">OB</option>
                <option value="Lainnya">Lainnya</option>
              </select>
            </div>
            <button class="btn btn-primary btn-lg"><i class="fa fa-search"></i> Cari Laporan</button>
          </form>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-body">
          <h5 class="mb-3">Laporan Rekap Jam</h5>
          <form action="{{ url('/laporan-rekap') }}" method="GET" target="_blank">
            <div class="mb-3">
              <label for="" class="form-label">Tanggal Mulai</label>
              <input type="date" class="form-control" name="dari" value="{{ old('tanggal') }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Tanggal Akhir</label>
              <input type="date" class="form-control" name="sampai" value="{{ old('tanggal') }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Cluster</label>
              <select type="text" class="form-select" name="cluster">
                <option value="">- Pilih -</option>
                <option value="Tetap">Tetap</option>
                <option value="Honorer">Honorer</option>
                <option value="Satpam">Satpam</option>
                <option value="OB">OB</option>
                <option value="Lainnya">Lainnya</option>
              </select>
            </div>
            <button class="btn btn-success btn-lg"><i class="fa fa-search"></i> Cari Laporan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#birthday').daterangepicker({
     singleDatePicker: true,
      locale: {
        format: 'DD-MM-YYYY'
      },
      maxDate: new Date(),
      calender_style: "picker_1"
    });
  });
</script> 
@endsection