<table class="table table-bordered table-hover">
   <thead>
      <tr>
         <th class="text-center" rowspan="2">NO</th>
         <th class="text-left" rowspan="2">Nama</th>
         <th class="text-center" colspan="2">1</th>
         <th class="text-center" colspan="2">2</th>
         <th class="text-center" colspan="2">3</th>
         <th class="text-center" colspan="2">4</th>
         <th class="text-center" colspan="2">5</th>
         <th class="text-center" colspan="2">6</th>
         <th class="text-center" colspan="2">7</th>
         <th class="text-center" colspan="2">8</th>
         <th class="text-center" colspan="2">9</th>
         <th class="text-center" colspan="2">10</th>
         <th class="text-center" colspan="2">11</th>
         <th class="text-center" colspan="2">12</th>
         <th class="text-center" colspan="2">13</th>
         <th class="text-center" colspan="2">14</th>
         <th class="text-center" colspan="2">15</th>
         <th class="text-center" colspan="2">16</th>
         <th class="text-center" colspan="2">17</th>
         <th class="text-center" colspan="2">18</th>
         <th class="text-center" colspan="2">19</th>
         <th class="text-center" colspan="2">20</th>
         <th class="text-center" colspan="2">21</th>
         <th class="text-center" colspan="2">22</th>
         <th class="text-center" colspan="2">23</th>
         <th class="text-center" colspan="2">24</th>
         <th class="text-center" colspan="2">25</th>
         <th class="text-center" colspan="2">26</th>
         <th class="text-center" colspan="2">27</th>
         <th class="text-center" colspan="2">28</th>
         <th class="text-center" colspan="2">29</th>
         <th class="text-center" colspan="2">30</th>
         <th class="text-center" colspan="2">31</th>
      </tr>
      <tr>
         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>

         <th class="text-center">IN</th>
         <th class="text-center">OUT</th>
      </tr>
   </thead>

   <tbody>
      <?php $no = 1; ?>
      @foreach($detail as $row)
      <tr>
         <td>{{$no}}</td>
         <td>{{$row->nama}}</td>
         <td align="center">{{$row->h1in}}</td>
         <td align="center">{{$row->h1out}}</td>
         <td align="center">{{$row->h2in}}</td>
         <td align="center">{{$row->h2out}}</td>
         <td align="center">{{$row->h3in}}</td>
         <td align="center">{{$row->h3out}}</td>
         <td align="center">{{$row->h4in}}</td>
         <td align="center">{{$row->h4out}}</td>
         <td align="center">{{$row->h5in}}</td>
         <td align="center">{{$row->h5out}}</td>
         <td align="center">{{$row->h6in}}</td>
         <td align="center">{{$row->h6out}}</td>
         <td align="center">{{$row->h7in}}</td>
         <td align="center">{{$row->h7out}}</td>
         <td align="center">{{$row->h8in}}</td>
         <td align="center">{{$row->h8out}}</td>
         <td align="center">{{$row->h9in}}</td>
         <td align="center">{{$row->h9out}}</td>
         <td align="center">{{$row->h10in}}</td>
         <td align="center">{{$row->h10out}}</td>
         <td align="center">{{$row->h11in}}</td>
         <td align="center">{{$row->h11out}}</td>
         <td align="center">{{$row->h12in}}</td>
         <td align="center">{{$row->h12out}}</td>
         <td align="center">{{$row->h13in}}</td>
         <td align="center">{{$row->h13out}}</td>
         <td align="center">{{$row->h14in}}</td>
         <td align="center">{{$row->h14out}}</td>
         <td align="center">{{$row->h15in}}</td>
         <td align="center">{{$row->h15out}}</td>
         <td align="center">{{$row->h16in}}</td>
         <td align="center">{{$row->h16out}}</td>
         <td align="center">{{$row->h17in}}</td>
         <td align="center">{{$row->h17out}}</td>
         <td align="center">{{$row->h18in}}</td>
         <td align="center">{{$row->h18out}}</td>
         <td align="center">{{$row->h19in}}</td>
         <td align="center">{{$row->h19out}}</td>
         <td align="center">{{$row->h20in}}</td>
         <td align="center">{{$row->h20out}}</td>
         <td align="center">{{$row->h21in}}</td>
         <td align="center">{{$row->h21out}}</td>
         <td align="center">{{$row->h22in}}</td>
         <td align="center">{{$row->h22out}}</td>
         <td align="center">{{$row->h23in}}</td>
         <td align="center">{{$row->h23out}}</td>
         <td align="center">{{$row->h24in}}</td>
         <td align="center">{{$row->h24out}}</td>
         <td align="center">{{$row->h25in}}</td>
         <td align="center">{{$row->h25out}}</td>
         <td align="center">{{$row->h26in}}</td>
         <td align="center">{{$row->h26out}}</td>
         <td align="center">{{$row->h27in}}</td>
         <td align="center">{{$row->h27out}}</td>
         <td align="center">{{$row->h28in}}</td>
         <td align="center">{{$row->h28out}}</td>
         <td align="center">{{$row->h29in}}</td>
         <td align="center">{{$row->h29out}}</td>
         <td align="center">{{$row->h30in}}</td>
         <td align="center">{{$row->h30out}}</td>
         <td align="center">{{$row->h31in}}</td>
         <td align="center">{{$row->h31out}}</td>
      </tr>
      <?php $no++; ?>
      @endforeach
   </tbody>
</table>