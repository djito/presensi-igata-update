@extends('layouts.app-admin')

@section('content')

<h1 class="mt-3">Edit User</h1>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{url('/manajemen-user')}}">Master User</a></li>
    <li class="breadcrumb-item active">Edit User</li>
</ol>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{url('manajemen-user')}}" class="text-dark text-decoration-none"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
            <div class="card-body">
                {!! Form::model($user, ['url' => ['/update-data', $user->id_user]]) !!}
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li class="active">
                                <center><i class="fa fa-key"></i> Data Login</center>
                            </li>
                        </ol>
                        <div class="form-group row mb-3">
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required="required">

                                @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="username" class="col-md-3 col-form-label text-md-right">{{ __('Username *') }}</label>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ $user->username }}" required autofocus>

                                @if ($errors->has('username'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password*') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ $user->password_view}}" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                                                
                        {{-- Departemen --}}
                        <div class="form-group row mb-3">
                            <label for="id_dep"
                                class="col-md-3 col-form-label">{{ __('Departemen *') }}</label>
                            <div class="col-md-6">
                                <select id="id_dep" type="text"
                                    class="form-select{{ $errors->has('id_dep') ? ' is-invalid' : '' }}"
                                    name="id_dep" value="{{ old('id_dep')}}" required>
                                    <option value="">- Pilih -</option>
                                    @foreach($departemen as $row)
                                        <option value="{{$row->id}}" <?php if ($user->id_dep == $row->id) {
                                            echo 'selected';
                                        } ?> >{{$row->nama_dep}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('id_dep'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('id_dep') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Jabatan --}}
                        <div class="form-group row mb-3">
                            <label for="jabatan"
                                class="col-md-3 col-form-label">{{ __('Jabatan *') }}</label>
                            <div class="col-md-6">
                                <select id="jabatan" type="text"
                                    class="form-select{{ $errors->has('jabatan') ? ' is-invalid' : '' }}"
                                    name="jabatan" value="{{ old('jabatan')}}" required>
                                    <option value="">- Pilih -</option>
                                    @foreach($jabatan as $row)
                                        <option value="{{$row->id}}" <?php if ($user->id_jabatan == $row->id) {
                                            echo 'selected';
                                        } ?> >{{$row->nama_jabatan}}-{{$row->keterangan}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('jabatan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('jabatan') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Atasan --}}
                        <div class="form-group row mb-3">
                            <label for="atasan1"
                                class="col-md-3 col-form-label">{{ __('Atasan *') }}</label>
                            <div class="col-md-6">
                                <select id="atasan1" type="text"
                                    class="form-select{{ $errors->has('atasan1') ? ' is-invalid' : '' }}"
                                    name="atasan1" value="{{ old('atasan1')}}" required>
                                    <option value="">- Pilih -</option>
                                    @foreach($jabatan as $row)
                                        <option value="{{$row->id}}" <?php if ($user->id_atasan1 == $row->id) {
                                            echo 'selected';
                                        } ?> >{{$row->nama_jabatan}}-{{$row->keterangan}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('atasan1'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('atasan1') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Level User --}}
                        <div class="form-group row mb-3" >
                            <label for="id_level" class="col-md-3 col-form-label text-md-right">{{ __('User Level *') }}</label>
                            <div class="col-md-6">
                                <select id="id_level" type="text" class="form-select{{ $errors->has('id_level') ? ' is-invalid' : '' }}" name="id_level" required autofocus>
                                    <option value="">- Pilih -</option>
                                    @foreach($level as $row)
                                        <option value="{{$row->id}}" <?php if ($user->id_level == $row->id) {
                                            echo 'selected';
                                        } ?> >{{$row->nama_level}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('id_level'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('id_level') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        {{-- Status --}}
                        <div class="form-group row mb-3">
                            <label for="status"
                                class="col-md-3 col-form-label">{{ __('Status *') }}</label>
                            <div class="col-md-6">
                                <select id="status" type="text"
                                    class="form-select{{ $errors->has('status') ? ' is-invalid' : '' }}"
                                    name="status" value="{{ old('status')}}" required>
                                    <option value="">- Pilih -</option>
                                    <option value="1" <?php if ($user->status == '1') {echo 'selected';} ?>>Aktif</option>
                                    <option value="0" <?php if ($user->status == '0') {echo 'selected';} ?>>Non Aktif</option>
                                </select>

                                @if ($errors->has('status'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li class="active">
                                <center><i class="fa fa-user"></i> Data Diri</center>
                            </li>
                        </ol>
                        <div class="form-group row mb-3">
                            <label for="nik" class="col-md-4 col-form-label text-md-right">{{ __('NIK*') }}</label>
                            <div class="col-md-8">
                                <input id="nik" type="number" min="0" class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}" name="nik" value="{{ $user->nik}}" disabled>

                                @if ($errors->has('nik'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('nik') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap *') }}</label>
                            <div class="col-md-8">
                                <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ $user->nama }}" required autofocus>

                                @if ($errors->has('nama'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('nama') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="gelar" class="col-md-4 col-form-label text-md-right">{{ __('Gelar') }}</label>
                            <div class="col-md-4">
                                <input id="gelar" type="text" min="0" class="form-control{{ $errors->has('gelar') ? ' is-invalid' : '' }}" name="gelar" value="{{ $user->gelar}}">

                                @if ($errors->has('gelar'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('gelar') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row mb-3">
                            <label for="alamat_ktp" class="col-md-4 col-form-label text-md-right">{{ __('Alamat KTP') }}</label>
                            <div class="col-md-8">
                                <input id="alamat_ktp" type="text" min="0" class="form-control{{ $errors->has('alamat_ktp') ? ' is-invalid' : '' }}" name="alamat_ktp" value="{{ $user->alamat_ktp}}">

                                @if ($errors->has('alamat_ktp'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('alamat_ktp') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="alamat_domisili" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Domisili') }}</label>
                            <div class="col-md-8">
                                <input id="alamat_domisili" type="text" min="0" class="form-control{{ $errors->has('alamat_domisili') ? ' is-invalid' : '' }}" name="alamat_domisili" value="{{ $user->alamat_domisili}}">

                                @if ($errors->has('alamat_domisili'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('alamat_domisili') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="no_hp" class="col-md-4 col-form-label text-md-right">Nomor Handphone</label>
                            <div class="col-md-8">
                                <input id="no_hp" type="number" min="0" class="form-control{{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="{{ $user->no_hp}}">

                                @if ($errors->has('no_hp'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('no_hp') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right"><i class="fas fa-save"></i> Simpan</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>

@endsection