@extends('layouts.app-admin')

@section('content')

<h2 class="mt-3">Detail User</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{url('/manajemen-user')}}">Master User</a></li>
    <li class="breadcrumb-item active">Detail User</li>
</ol>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header">
                <a href="{{url('manajemen-user')}}" class="text-dark text-decoration-none">
                <i class="fas fa-arrow-left"></i>&ensp;Kembali</a>
            </div>
            <div class="card-body">
                @foreach($data as $row)                
                <div class="row">
                    <div class="col-md-6">

                        <h5 class="mb-3"><i class="fa fa-key"></i> Data Login</h5>
                          
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label">{{ __('E-Mail') }}</label>
                            <div class="col-md-8">
                                : {{$row->email}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-md-3 col-form-label">{{ __('Username') }}</label>
                            <div class="col-md-8">
                                : {{$row->username}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-8">
                                : {{$row->password_view}}
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="level" class="col-md-3 col-form-label text-md-right">{{ __('Departemen') }}</label>
                            <div class="col-md-6">
                                : {{$row->nama_dep}}                               
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="level" class="col-md-3 col-form-label text-md-right">{{ __('Jabatan') }}</label>
                            <div class="col-md-6">
                                : {{$row->jabatan}} - {{$row->ket_jabatan}}                            
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="level" class="col-md-3 col-form-label text-md-right">{{ __('Atasan') }}</label>
                            <div class="col-md-6">
                                : {{$row->atasan1}} - {{$row->atasan2}}                               
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="level" class="col-md-3 col-form-label text-md-right">{{ __('User Level') }}</label>
                            <div class="col-md-6">
                                : {{$row->nama_level}}                               
                            </div>
                        </div>
                        <div class="form-group row mb-3">
                            <label for="level" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>
                            <div class="col-md-6">
                                                  
                                <?php if ($row->status == '1') {
                                    echo ': Aktif';
                                } else {
                                    echo ': Non Aktif';
                                } ?>              
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                       
                        <h5 class="mb-3"><i class="fa fa-user"></i> Data Diri</h5>

                        <div class="form-group row">
                            <label for="nik" class="col-md-4 col-form-label">{{ __('NIK') }}</label>
                            <div class="col-md-8">
                                : {{$row->nik}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label">{{ __('Nama Lengkap') }}</label>
                            <div class="col-md-8">
                                : {{$row->nama}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gelar" class="col-md-4 col-form-label">{{ __('Gelar') }}</label>
                            <div class="col-md-4">
                                : {{$row->gelar}}
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="alamat_ktp" class="col-md-4 col-form-label">{{ __('Alamat KTP') }}</label>
                            <div class="col-md-8">
                                : {{$row->alamat_ktp}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat_domisili" class="col-md-4 col-form-label">{{ __('Alamat Domisili') }}</label>
                            <div class="col-md-8">
                                : {{$row->alamat_domisili}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_hp" class="col-md-4 col-form-label">Nomor Handphone</label>
                            <div class="col-md-8">
                               : {{$row->no_hp}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            @endforeach
        </div>
    </div>
</div>

@endsection