@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Daftar Pengajuan Cuti</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Daftar Pengajuan Cuti</li>
</ol>
<div class="row">
    <div class="col-xl-12">

        {{-- Pengajuan Cuti --}}
        <div class="card-header">
            {!! Form::open(['files'=>true, 'url' => ['/simpan-pengajuan-cuti']]) !!}

            {{-- Baris 1 --}}
            <div class="row">
                {{-- ALasan --}}
                <div class="col-sm-5">                                           
                    <div class="form-group">                          
                        <label class="col-md-7 col-sm-7 col-xs-12 ">Alasan Pengajuan Cuti*</label>                        
                        <textarea type="text" class="form-control" name="alasan" rows="4"></textarea>                        
                    </div> 
                </div>

                <div class="col-sm-3">                    
                    <!-- Dari Tanggal -->
                    <div class="form-group">
                        <label>Dari Tanggal</label>                                      
                        <div class="form-group">   
                            <input type="date" class="form-control" name="dari_tanggal" value="{{old('tanggal') }}" required >
                        </div>                                      
                    </div>
                    <!-- Sampai Tanggal -->
                    <div class="form-group">
                        <label>Sampai Tanggal</label>                                      
                        <div class="form-group">   
                            <input type="date" class="form-control" name="sampai_tanggal" value="{{old('tanggal') }}" required >
                        </div>                                      
                    </div>
                </div>            
            </div>

            {{-- Baris 2 --}}
            <div class="row">

                {{-- Jenis Cuti --}}
                <div class="col-sm-2">
                    <div class="form-group">                            
                        <label>Jenis Cuti</label>                             
                        <select name="id_cuti" id="id_cuti" class="form-select" required >
                            <option value="">- Pilih -</option>
                            @foreach($jeniscuti as $row)
                                <option value="{{$row->id}}">{{$row->jenis_cuti}}</option>
                            @endforeach
                        </select>
                    </div> 
                </div>
                
                {{-- Nama Pengganti --}}
                <div class="col-sm-2">
                    <!-- Date dd/mm/yyyy -->
                    <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12 ">Pilih Nama Pengganti</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="id_pengganti" class="form-select">
                            <option value="">- Pilih -</option>
                            @foreach($nama as $row)
                            <option value="{{$row->id_user}}">{{$row->nama}}</option>
                            @endforeach
                            </select>
                        </div>                                     
                    </div> 
                </div>   
                
                {{-- Lampiran --}}
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Lampiran Pendukung (.PDF)</label>    
                        <div class="form-group mb-3">                            
                              <input type="file" name="lampiran" class="form-control">                                                      
                        </div>
                    </div>
                </div>

                {{-- Tombol Tambah Usbmit --}}
                <div class="col-sm-2 mt-4"> 
                    <div class="form-group">                       
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button>  
                    </div>                      
                </div> 
            </div>
            {!! Form::close() !!}                 
            
        </div>
        {{-- End Tambah --}}

        {{-- Tabel Daftar Cuti --}}
        <div class="card mb-12">
            <div class="card-body">
                {!! Form::open(['url' => 'batal-pengajuan-cuti/{id}']) !!}
                <div class="table-responsive">
                    <table id="dataTable2">
                        <thead>
                            <tr>
                                <th width="2%" class="text-center"><input type="checkbox" name="select_all" id="select_all" value=""/></th>
                                <th class="text-center">No</th>
                                <th class="text-center">Dari Tanggal</th>
                                <th class="text-center">Sampai Tanggal</th>
                                <th class="text-center">Alasan</th>
                                <th class="text-center">Jenis Cuti</th>
                                <th class="text-center">File</th>
                                <th class="text-center">Pengganti</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($data as $row)
                            @if($row->status == 'pengajuan')
                            <tr>
                            @elseif($row->status == 'di terima')
                            <tr class="bg-success" style="color: rgb(83, 231, 150);">
                            @elseif($row->status == 'di tolak')
                            <tr class="bg-danger" style="color: rgb(235, 92, 92);">
                            @endif
                                <td align="center"><label class="checkbox-inline"><input type="checkbox" name="checked_id[]" class="checkbox" value="{{$row->id}}"/></label></td>
                                <td align="center">{{$no}}</td>
                                <td align="center">{{date_format(date_create($row->dari_tanggal),"d F Y")}}</td>
                                <td align="center">{{date_format(date_create($row->sampai_tanggal),"d F Y")}}</td>
                                <td>{!!$row->alasan!!}</td>
                                <td>{!!$row->jenis_cuti!!}</td>
                                <td align="center"><a class="text-light" href="{{asset('/assets/file/cuti')}}/{{$row->bukti_pendukung}}" target="_blank" ><i class="fa fa-download"></i></a></td>
                                <td>{{$row->nama}}</td>
                                <td class="bg-success" style="color: rgb(83, 231, 150);">{{$row->status}}<br>{{$row->disetujui}}</td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <input type="submit" class="btn btn-danger" name="batal_pengajuan" onclick="return check()" value="BATAL PENGAJUAN"/>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#select_all').on('click',function(){
      if(this.checked){
        $('.checkbox').each(function(){
          this.checked = true;
      });
    }else{
     $('.checkbox').each(function(){
        this.checked = false;
    });
 }
});
});

  $('input[type="checkbox"]').on('change', function() {
    $(this).closest('td').find('input').not(this).prop('checked', false);
});
</script>
@endsection