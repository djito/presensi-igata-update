@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Daftar Pengajuan Lembur</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Daftar Pengajuan Lembur</li>
</ol> 
<div class="row">
    <div class="col-xl-12">

        {{-- Pengajuan Cuti --}}
        <div class="card-header">
            {!! Form::open(['files'=>true, 'url' => ['/simpan-pengajuan-lembur']]) !!}

                {{-- Baris 1 --}}
                <div class="row">            
                    <div class="col-sm-2">    
                        <!-- Sampai Tanggal -->
                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group mb-3"> 
                                <input type="date" class="form-control rounded-0" id="tanggal" name="tanggal" onkeypress="return number(event)" value="{{old('tanggal') }}" placeholder="Tanggal" >
                                
                                <button title="Edit" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal-xl"> <i class="fa fa-edit"></i></button>
                                <input id="id" name="id" type="text" hidden class="form-control" >
                                
                            </div>                                   
                        </div>
                    </div>      
                    <div class="col-sm-2">    
                        <!-- Shift -->
                        <div class="form-group">
                            <label>Shift</label>                                      
                            <div class="form-group">   
                                {{-- <input type="text" class="form-control" name="shift" value="{{old('shift') }}" disabled > --}}
                                <select name="shift" class="form-select">
                                    <option value="">- Pilih -</option>
                                    <option value="1">Pagi</option>
                                    <option value="2">Siang</option>
                                    <option value="3">Malam</option>
                                    <option value="0">Off</option>
                                </select>
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label>Jam Masuk</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jammasuk" value="{{old('jammasuk') }}" disabled >
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label>Jam Pulang</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jampulang" value="{{old('jampulang') }}" disabled >
                            </div>                                       
                        </div>
                    </div>        
                </div>

            
                <div class="row">
                    <label style="margin:20px;">
                        KTP : <br />
                        <select id="ktp" name="ktp" style="margin-left:20px;">
                            <option value="Tidak Ada">Tidak Ada</option>
                            <option value="Ada">Ada</option>
                        </select>
                        
                            <input type="text" name="no_ktp" id="no_ktp" value="" hidden />
                    </label>

                    {{-- Jenis Cuti --}}
                    <div class="col-sm-2">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label>Jam Masuk</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jammasuk" value="{{old('jammasuk') }}" disabled >
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label>Jam Pulang</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jampulang" value="{{old('jampulang') }}" disabled >
                            </div>                                       
                        </div>
                    </div> 
                    
                </div>                

            

                <div class="row">

                    {{-- Jenis Cuti --}}
                    <div class="col-sm-2">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label>Jam Masuk</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jammasuk" value="{{old('jammasuk') }}" disabled >
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label>Jam Pulang</label>                                      
                            <div class="form-group">   
                                <input type="date" class="form-control" name="jampulang" value="{{old('jampulang') }}" disabled >
                            </div>                                       
                        </div>
                    </div> 
                    
                </div>  
                    
            
                {{-- Baris 2 --}}
                <div class="row">

                    {{-- Jenis Cuti --}}
                    <div class="col-sm-2">
                        <div class="form-group">                            
                            <label>Kompensasi</label>                             
                            <select name="jenis_izin" class="form-select">
                                <option value="">- Pilih -</option>
                                <option value="1">Bayar Lembur</option>
                                <option value="2">Ganti Cuti</option>
                            </select>
                        </div> 
                    </div>
                    
                    {{-- Nama Pengganti --}}
                    <div class="col-sm-2">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12 ">Pilih Nama Pengganti</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="id_pengganti" class="form-select">
                                <option value="">- Pilih -</option>
                            {{--  @foreach($nama as $row)
                                <option value="{{$row->id_user}}">{{$row->nama}}</option>
                                @endforeach --}}
                                </select>
                            </div>                                     
                        </div> 
                    </div>   
                    
                    {{-- Lampiran --}}
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Lampiran Pendukung (.PDF)</label>    
                            <div class="form-group mb-3">                            
                                <input type="file" name="lampiran" class="form-control">                                                      
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div>
                            <label><input type="radio" name="type" value="single" />Single</label>
                            <label><input type="radio" name="type" value="team" />Team</label>
                        </div>
                        <div class="single select">
                            <label style="color: black">Your Name:</label>
                            <input type="text" placeholder="Your Name" />
                        </div>
                        <div class="team select">
                            <label style="color: black">Your Team Name:</label>
                            <input type="text" placeholder="Your Team Name" />
                        </div>
        
                    </div>

                    {{-- Tombol Tambah Usbmit --}}
                    <div class="col-sm-2 mt-4"> 
                        <div class="form-group">                       
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button>  
                        </div>                      
                    </div> 
                </div>

            
            {!! Form::close() !!}                 
            
        </div>
        {{-- End Tambah --}}

        <div class="card mb-12">
            <div class="card-body">
                {!! Form::open(['url' => 'batal-pengajuan-izin/{id}']) !!}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-bordered table-hover" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="2%" class="text-center"><input type="checkbox" name="select_all" id="select_all" value=""/></th>
                                <th class="text-center">No</th>
                                <th class="text-center">Dari Tanggal</th>
                                <th class="text-center">Sampai Tanggal</th>
                                <th class="text-center">Alasan</th>
                                <th class="text-center">Jenis izin</th>
                                <th class="text-center">File</th>
                                <th class="text-center">Pengganti</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                           {{--  @foreach($data as $row)
                            @if($row->status == 'pengajuan')
                            <tr>
                            @elseif($row->status == 'di terima')
                            <tr class="bg-success" style="color: white;">
                            @elseif($row->status == 'di tolak')
                            <tr class="bg-danger" style="color: white;">
                            @endif
                                <td align="center"><label class="checkbox-inline"><input type="checkbox" name="checked_id[]" class="checkbox" value="{{$row->id}}"/></label></td>
                                <td align="center">{{$no}}</td>
                                <td align="center">{{date_format(date_create($row->dari_tanggal),"d F Y")}}</td>
                                <td align="center">{{date_format(date_create($row->sampai_tanggal),"d F Y")}}</td>
                                <td>{!!$row->alasan!!}</td>
                                <td>
                                    @if($row->jenis_izin == 'D')
                                    Dinas
                                    @elseif($row->jenis_izin == ']I2')
                                    Tidak presensi pulang
                                    @elseif($row->jenis_izin == 'I1')
                                    Izin Telat
                                    @endif
                                </td>
                                <td align="center"><a class="text-light" href="{{asset('/assets/file/izin/')}}/{{$row->bukti_pendukung}}" target="_blank" ><i class="fa fa-download"></i></a></td>
                                <td>{{$row->nama}}</td>
                                <td>{{$row->status}}<br>{{$row->disetujui}}</td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach --}}
                        </tbody>
                    </table>

                    <div class="row">
                        <label style="margin:20px;">
                            KTP : <br />
                            <select id="ktp" name="ktp" style="margin-left:20px;">
                                <option value="Tidak Ada">Tidak Ada</option>
                                <option value="Ada">Ada</option>
                            </select>
                            
                                <input type="text" name="no_ktp" id="no_ktp" value="" hidden />
                        </label>
    
                        {{-- Jenis Cuti --}}
                        <div class="col-sm-2">    
                            <!-- Jam Masuk -->
                            <div class="form-group">
                                <label>Jam Masuk</label>                                      
                                <div class="form-group">   
                                    <input type="date" class="form-control" name="jammasuk" value="{{old('jammasuk') }}" disabled >
                                </div>                                      
                            </div>
                        </div>  
                        <div class="col-sm-2">    
                            <!-- Jam Pulang -->
                            <div class="form-group">
                                <label>Jam Pulang</label>                                      
                                <div class="form-group">   
                                    <input type="date" class="form-control" name="jampulang" value="{{old('jampulang') }}" disabled >
                                </div>                                       
                            </div>
                        </div> 
                        
                    </div>  
                </div>
                <input type="submit" class="btn btn-danger" name="batal_pengajuan" onclick="return check();" value="BATAL PENGAJUAN"/>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>



{{-- Modal --}}
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data DPT PILWAKOT 2020</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
                <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg"> 
                        {{-- Tabel Simpatisan --}}
                        <table id="tabel2" class="table table-striped" >
                            <thead>
                                <tr>
                                    <th>KK</th>
                                    <th>NIK</th>
                                    <th>Nama</th> 
                                    <th>Tempat Lahir</th> 
                                    <th>Tgl Lahir</th>                        
                                    <th>JK</th> 
                                    <th>Alamat</th>                         
                                    <th>RT</th>
                                    <th>RW</th>
                                    <th>Kelurahan</th>
                                    <th class="uk-text-nowrap">Aksi</th>
                                </tr>
                            </thead>   
                            <tbody>

                                {{-- @foreach($datadpt as $dpt)
                                    <tr>
                                        <td>{{$dpt->kk}}</td>
                                        <td>{{$dpt->nik}}</td>
                                        <td>{{$dpt->nama}}</td> 
                                        <td>{{$dpt->tempat_lahir}}</td> 
                                        <td>{{$dpt->tgl_lahir}}</td>                        
                                        <td>{{$dpt->jenis_kelamin}}</td>
                                        <td>{{$dpt->alamat}}</td>                                                             
                                        <td>{{$dpt->rt}}</td>
                                        <td>{{$dpt->rw}}</td>
                                        <td>{{ucfirst(trans($dpt->kelurahan))}}</td>                                        
                                        <td>
                                            <button class="btn btn-xs btn-info" id="pilih"                                                
                                                data-kk="<?=$dpt->kk?>"
                                                data-nik="<?=$dpt->nik?>"
                                                data-nama="<?=$dpt->nama?>"
                                                data-tempat_lahir="<?=$dpt->tempat_lahir?>"
                                                data-tgl_lahir="<?=$dpt->tgl_lahir?>"
                                                data-jenis_kelamin="<?=$dpt->jenis_kelamin?>"
                                                data-alamat="<?=$dpt->alamat?>"
                                                data-rt="<?=substr($dpt->rt,1)?>"
                                                data-rw="<?=substr($dpt->rw,1)?>"
                                                data-kelurahan="<?=rtrim(strtolower($dpt->kelurahan))?>" >
                                                <i class="fa fa-check"></i>Pilih
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach           --}}                                  
                            </tbody>
                        </table>
                        {{-- End Tabel simpatisan --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer right-content-between">
                <button type="button" id="btn-tutup1" class="btn btn-default" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{-- End MOdal --}}


{{-- Modal edit shift --}}
  

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">   
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Shift Kerja</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">    
                <h5 class="modal-title">Edit Shift Kerja</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
            </div>
        </div> 
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
            this.checked = true;
        });
        }else{
        $('.checkbox').each(function(){
            this.checked = false;
            });
            }
        });

        $('input[type="radio"]').click(function () {
        var inputValue = $(this).attr("value");
        var target = $("." + inputValue);
        $(".select").not(target).hide();
        $(target).show();
        });

    });

    $('input[type="checkbox"]').on('change', function() {
        $(this).closest('td').find('input').not(this).prop('checked', false);
    });

    $("#ktp").change(function() {
        console.log($("#ktp option:selected").val());
        if ($("#ktp option:selected").val() == 'Tidak Ada') {
            $('#no_ktp').prop('hidden', 'true');
        } else {
            $('#no_ktp').prop('hidden', false);
        }
    });

</script>

@endsection