@extends('layouts.app-admin')
@section('content')
<h2 class="mt-3">Daftar Pengajuan Lembur</h2>
<ol class="breadcrumb mb-3">
    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
    <li class="breadcrumb-item active">Daftar Pengajuan Lembur</li>
</ol> 
<div class="row">
    <div class="col-xl-12">

        {{-- Pengajuan Cuti --}}
        <div class="card-header">
            {!! Form::open(['files'=>true, 'url' => ['/simpan-pengajuan-lembur']]) !!}

                {{-- Baris 1 --}}
                <div class="row">            
                    <div class="col-sm-4">    
                        <!-- Sampai Tanggal -->
                        <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group mb-3"> 
                                {{-- <input type="date" class="form-control rounded-0" id="tanggal" name="tanggal" onkeypress="return number(event)" value="{{old('tanggal') }}" placeholder="Tanggal" >
                                
                                <button title="Edit" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modal-xl"> <i class="fa fa-edit"></i></button>
                                <input id="id" name="id" type="text" hidden class="form-control" > --}}

                                <select name="tanggal" id="tanggal" class="form-select" required>
                                    <option value="">- Pilih -</option>
                                    @foreach($jamkerja as $row)
                                    <option value="{{$row->tglmasuk}}" 
                                        data-jammasuk="<?=$row->jammasuk?>"
                                        data-jampulang="<?=$row->jampulang?>"
                                        data-nama_shift="<?=$row->nama_shift?>"
                                        data-id_jam="<?=$row->id?>"
                                        >{{$row->tglmasuk}} - <b>{{$row->nama_shift}}</b> ( {{$row->jammasuk}}-{{$row->jampulang}} - {{$row->wf}} )</option>
                                    @endforeach
                                    </select>
                                
                            </div>                                   
                        </div>
                    </div>      
                    <div class="col-sm-1">    
                        <!-- Shift -->
                        <div class="form-group">
                            <label>Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="nama_shift" id="nama_shift" value="{{old('nama_shift') }}" disabled >
                                <input type="text" class="form-control" name="id_jam"  id="id_jam" value="{{old('id_jam') }}" hidden >                                
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label>Jam Masuk</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="jammasuk" id="jammasuk" value="00:00" disabled >
                            </div>                                      
                        </div>
                    </div>  
                    <div class="col-sm-2">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label>Jam Pulang</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="jampulang" id="jampulang"value="00:00" disabled >
                            </div>                                       
                        </div>
                    </div>        
                </div>             
            
                <div class="row my-0"> 
                    <div class="Off select col-sm-2 my-0">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label style="color: black">Jam Masuk</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="jammasuk1" id="jammasuk1" {{-- onkeyup="sumtime();" --}} value="00:00:00"  >
                            </div>                                      
                        </div>
                    </div>  
                    <div class="Off select  col-sm-2 my-0">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label style="color: black">Jam Pulang</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="jampulang1" id="jampulang1" {{-- onkeyup="sumtime();" --}} value="00:00:00"  >
                            </div>                                       
                        </div>
                    </div> 

                    {{-- Pagi --}}
                    <div class="Pagi select col-sm-2">    
                        <!-- Jam Masuk -->
                        <div class="form-group">
                            <label style="color: black">Sebelum Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="sebelum_shift" id="sebelum_shift" value="00:00"  >
                            </div>                                      
                        </div>
                        <div class="form-group">
                            <label style="color: black">Istirahat Sebelum Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="istirahat_sebelum" id="istirahat_sebelum" value="00:00"  >
                            </div>                                      
                        </div>
                    </div>  
                    
                    <div class="Pagi select  col-sm-2">    
                        <!-- Jam Pulang -->
                        <div class="form-group">
                            <label style="color: black">Setelah Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="setelah_shift"  id="setelah_shift" value="00:00"  >
                            </div>                                       
                        </div>
                        <div class="form-group">
                            <label style="color: black">Istirahat Setelah Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="istirahat_setelah" id="istirahat_setelah" value="00:00"  >
                            </div>
                        </div>                    
                    </div>
                    {{-- Pagi End --}}

                    {{-- Malam --}}
                    {{-- <div class="Malam select col-sm-2">    
                        
                        <div class="form-group">
                            <label style="color: black">Sebelum Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="sebelum_shift" id="sebelum_shift" value="00:00"  >
                            </div>                                      
                        </div>
                        <div class="form-group">
                            <label style="color: black">Istirahat Sebelum Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="istirahat_sebelum" id="istirahat_sebelum" value="00:00"  >
                            </div>                                      
                        </div>
                    </div>  
                    
                    <div class="Malam select  col-sm-2">    
                        
                        <div class="form-group">
                            <label style="color: black">Setelah Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="setelah_shift" id="setelah_shift" value="00:00"  >
                            </div>                                       
                        </div>
                        <div class="form-group">
                            <label style="color: black">Istirahat Setelah Shift</label>                                      
                            <div class="form-group">   
                                <input type="text" class="form-control" name="istirahat_setelah" id="istirahat_setelah" value="00:00"  >
                            </div>
                        </div>                    
                    </div> --}}
                    {{-- Malam End --}}

                    {{-- Jenis Cuti --}}
                    <div class="col-sm-2">
                        <div class="form-group">                            
                            <label>Kompensasi</label>                             
                            <select name="kompensasi" class="form-select" required>
                                <option value="">- Pilih -</option>
                                <option value="1">Bayar Lembur</option>
                                <option value="2">Ganti Cuti</option>
                            </select>
                        </div> 

                        <div class="form-group">
                            <label>Jumlah Lembur</label>
                            <div class="form-group">   
                                <input type="text" class="form-control" name="jumlah_lembur" id="jumlah_lembur" value="00:00" readonly  >
                            </div>          
                        </div> 
                    </div>
                    
                    {{-- Nama Pengganti --}}
                   
                    
                    {{-- Lampiran --}}
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Lampiran Pendukung (.PDF)</label>    
                            <div class="form-group">                            
                                <input type="file" name="lampiran" class="form-control">                                                      
                            </div>
                        </div>
                       
                        <div class="form-group mt-4">                       
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>Tambah</button>  
                        </div>                      
                        
                    </div>
                    
                </div>     
                

            
            {!! Form::close() !!}                 
            
        </div>
        {{-- End Tambah --}}

        <div class="card mb-12">
            <div class="card-body">
                {!! Form::open(['url' => 'batal-pengajuan-lembur/{id}']) !!}
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-bordered table-hover" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="2%" class="text-center"><input type="checkbox" name="select_all" id="select_all" value=""/></th>
                                <th class="text-center">No</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Jam Masuk</th>
                                <th class="text-center">Jam Pulang</th>
                                <th class="text-center">Sebelum Shift</th>
                                <th class="text-center">Istirahat</th>
                                <th class="text-center">Setelah Shift</th>
                                <th class="text-center">Istirahat</th>
                                <th class="text-center">Jumlah Jam Lembur</th>
                                <th class="text-center">Kompensasi</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($data as $row)
                            @if($row->status == 'pengajuan')
                            <tr>
                            @elseif($row->status == 'di terima')
                            <tr class="bg-success" style="color: white;">
                            @elseif($row->status == 'di tolak')
                            <tr class="bg-danger" style="color: white;">
                            @endif
                                <td align="center"><label class="checkbox-inline"><input type="checkbox" name="checked_id[]" class="checkbox" value="{{$row->id}}"/></label></td>
                                <td align="center">{{$no}}</td>
                                <td align="center">{{date_format(date_create($row->tgllembur),"d F Y")}}</td>                                
                                <td>{!!$row->jammasuk!!}</td>
                                <td>{!!$row->jampulang!!}</td>
                                <td>{!!$row->sebelum_shift!!}</td>
                                <td>{!!$row->istirahat_sebelum!!}</td>
                                <td>{!!$row->setelah_shift!!}</td>  
                                <td>{!!$row->istirahat_setelah!!}</td> 
                                <td>{!!$row->jumlah_lembur!!}</td>                                     
                                <td>
                                    @if($row->kompensasi == '1')
                                    Bayar Lembur
                                    @elseif($row->kompensasi == '2')
                                    Ganti Cuti                                    
                                    @endif
                                </td>   
                                <td>{!!$row->status!!}<br>{!!$row->disetujui!!}</td>                                  
                                {{-- <td align="center"><a class="text-light" href="{{asset('/assets/file/izin/')}}/{{$row->bukti_pendukung}}" target="_blank" ><i class="fa fa-download"></i></a></td> --}}
                                
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>

                    
                </div>
                <input type="submit" class="btn btn-danger" name="batal_pengajuan" onclick="return check();" value="BATAL PENGAJUAN"/>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>



{{-- Mulai : <input type="text" id="waktuMulai" value="00:00"><br>
Selesai : <input type="text" id="waktuSelesai" value="00:00"><br>
Selisih : <input type="text" id="selisih"> --}}



<script type="text/javascript">
    $(document).ready(function(){
        $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
            this.checked = true;
        });
        }else{
        $('.checkbox').each(function(){
            this.checked = false;
            });
            }
        });

       /*  $('input[type="radio"]').click(function () {
        var inputValue = $(this).attr("value");
        var target = $("." + inputValue);
        $(".select").not(target).hide();
        $(target).show();
        }); */

    });

    $('input[type="checkbox"]').on('change', function() {
        $(this).closest('td').find('input').not(this).prop('checked', false);
    });

    /* $("#ktp").change(function() {
        console.log($("#ktp option:selected").val());
        if ($("#ktp option:selected").val() == 'Tidak Ada') {
            $('#no_ktp').prop('hidden', 'true');
        } else {
            $('#no_ktp').prop('hidden', false);
        }
    }); */

    $(document).on('click', '#pilih', function() {                
                var tglmasuk = $(this).data('tglmasuk');
                var jammasuk = $(this).data('jammasuk');
                var jampulang = $(this).data('jampulang');
                   
                
                $('#tglmasuk').val(tglmasuk);
                $('#jampulang').val(jampulang);
                $('#jammasuk').val(jammasuk)  ;              
                                                 
                $('#btn-tutup1').click();
    });

    $('#tanggal').on('change', function() {
        const selected = $(this).find('option:selected');        
        const jammasuk = selected.data('jammasuk');
        const jampulang = selected.data('jampulang'); 
        const nama_shift = selected.data('nama_shift'); 
        const id_jam = selected.data('id_jam');    

        var inputValue = selected.data('nama_shift');
        var target = $("." + inputValue);        
        $(".select").not(target).hide();
        $(target).show();
        
        
        $('#jammasuk').val(jammasuk);
        $('#jampulang').val(jampulang);
        $('#nama_shift').val(nama_shift);
        $('#id_jam').val(id_jam);        

    });

   /*  function sum(){
        var jam1 = document.getElementById('jammasuk1').value;
        var jam2 = document.getElementById('jampulang1').value;
        var hasil = parseInt(jam2)-parseInt(jam1);
        if (!isNaN(hasil)){
            document.getElementById('jumlah_lembur').value = hasil;
        }
    } */

    function sumtime(){
        var jam1 = document.getElementById('jammasuk1').value;
        var jam2 = document.getElementById('jampulang1').value;
        var hasil = jam2.getTime()-jam1.getTime();
        if (!isNaN(hasil)){
            document.getElementById('jumlah_lembur').value = hasil;
        }
    }

   
    $("input").keyup(function(){
      var waktuMulai = $('#jammasuk1').val(),
          waktuSelesai = $('#jampulang1').val(),
          sebelum_shift = $('#sebelum_shift').val(),
          jammasuk = $('#jammasuk').val(), 
          istirahat1 = $('#istirahat_sebelum').val(),          
          jampulang = $('#jampulang').val(),
          setelah_shift = $('#setelah_shift').val(),           
          istirahat2 = $('#istirahat_setelah').val();        

        if (waktuMulai!=0) {
            hours = waktuSelesai.split(':')[0] - waktuMulai.split(':')[0];
            minutes = waktuSelesai.split(':')[1] - waktuMulai.split(':')[1];
            if (waktuMulai <= "12:00:00" && waktuSelesai >= "13:00:00"){
                a = 1;
            }else {
                a = 0;
            }
            minutes = minutes.toString().length<2?'0'+minutes:minutes;
            if(minutes<0){ 
                hours--;
                minutes = 60 + minutes;        
            }
            hours = hours.toString().length<2?'0'+hours:hours;
        
            $('#jumlah_lembur').val(hours-a+ ':' + minutes);
        }
 
        else if(sebelum_shift!=0 || setelah_shift!=0) {

            hours = parseInt(jammasuk.substr(0,2)) - parseInt(sebelum_shift.substr(0,2)) -  parseInt(istirahat1.substr(0,2)) + parseInt(setelah_shift.substr(0,2)) - parseInt(jampulang.substr(0,2)) -  parseInt(istirahat2.substr(0,2)) ;
            minutes =  jammasuk.substr(3,2) - sebelum_shift.substr(3,2) -  istirahat1.substr(3,2) + setelah_shift.substr(3,2) - jampulang.substr(3,2) -  istirahat2.substr(3,2);

            /* hours = jammasuk.split(':')[0] - sebelum_shift.split(':')[0] -  istirahat1.split(':')[0] + setelah_shift.split(':')[0] - jampulang.split(':')[0] -  istirahat2.split(':')[0] ;
            minutes =  jammasuk.split(':')[1] - sebelum_shift.split(':')[1] -  istirahat1.split(':')[1] + setelah_shift.split(':')[1] - jampulang.split(':')[1] -  istirahat2.split(':')[1];
            */


            /*  hours1 =  setelah_shift.split(':')[0] - jampulang.split(':')[0] -  istirahat2.split(':')[0];
            minutes1 =  setelah_shift.split(':')[1] - jampulang.split(':')[1] -  istirahat2.split(':')[1] ; */

            minutes = minutes.toString().length<2?'0'+minutes:minutes;          
            if(minutes<0){ 
                hours--;
                minutes = 60 + minutes;        
            }
            hours = hours.toString().length<2?'0'+hours:hours;

            /* minutes1 = minutes1.toString().length<2?'0'+minutes1:minutes1;
            if(minutes1<0){ 
                hours1--;
                minutes1 = 60 + minutes1;        
            }
            hours1 = hours1.toString().length<2?'0'+hours1:hours1; */

          /*   jam = parseFloat(hours) + parseFloat(hours1) ;
            minut = parseFloat(minutes) + parseFloat(minutes1) ; */

            $('#jumlah_lembur').val(hours + ':' + minutes);

        } else {
            $('#jumlah_lembur').val('none');
        }
    });
  
</script>

@endsection